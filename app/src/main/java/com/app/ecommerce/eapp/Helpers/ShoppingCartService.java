package com.app.ecommerce.eapp.Helpers;

import com.app.ecommerce.eapp.Models.OrdersDetailResponse;
import com.app.ecommerce.eapp.Models.OrdersResponse;
import com.app.ecommerce.eapp.Models.ShoppingCartResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by RFI on 24-10-2017.
 */

public interface ShoppingCartService {
    @GET("api/ShoppingCart/GetShoppingCart")
    Call<ShoppingCartResponse> GetShoppingCart(@Query("CustomerID") int CustomerID, @Query("ShoppingcarttypeID") int ShoppingcarttypeID);

    @POST("api/ShoppingCart/AddToCart")
    Call<ResponseBody> AddToCart(@Query("productId") int productId,
                                 @Query("shoppingCartTypeId") int shoppingCartTypeId,
                                 @Query("quantity") int quantity,
                                 @Query("CustomerID") int CustomerID);

    @GET("api/ShoppingCart/UpdateShoppingCartApp")
    Call<ShoppingCartResponse> UpdateShoppingCartApp(@Query("productId") String productId,
                                                  @Query("ShoppingCartTypeId") int ShoppingCartTypeId,
                                                  @Query("quantity") int quantity,
                                                  @Query("CustomerID") int CustomerID,
                                                  @Query("Removefromcart") boolean Removefromcart);

    @GET("api/Order/GetCustomerOrders")
    Call<OrdersResponse> GetCustomerOrders(@Query("CustomerID") int CustomerID);

    @GET("api/Order/placeOrder")
    Call<Void> placeOrder(@Query("CustomerID") int CustomerID);

    @GET("api/Order/GetSingleOrder")
    Call<OrdersDetailResponse> GetSingleOrder(@Query("OrderID") int OrderID);

}
