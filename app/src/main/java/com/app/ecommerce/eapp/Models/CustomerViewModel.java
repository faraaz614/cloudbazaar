package com.app.ecommerce.eapp.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RFI on 25-10-2017.
 */

public class CustomerViewModel {
    @SerializedName("Id")
    public int Id;
    public int Id() {
        return Id;
    }

    @SerializedName("Username")
    public String Username;
    public String Username() { return Username; }

    @SerializedName("Email")
    public String Email;
    public String Email() { return Email; }

    @SerializedName("Password")
    public String Password;
    public String Password() { return Password; }

    @SerializedName("Mobile")
    public String Mobile;
    public String Mobile() { return Mobile; }
}
