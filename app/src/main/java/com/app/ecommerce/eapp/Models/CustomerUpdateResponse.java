package com.app.ecommerce.eapp.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RFI on 09-11-2017.
 */

public class CustomerUpdateResponse {
    @SerializedName("IsSuccess")
    private boolean IsSuccess;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private int Data;

    public boolean IsSuccess() {
        return IsSuccess;
    }

    public String Message() {
        return Message;
    }

    public int Data() {
        return Data;
    }
}
