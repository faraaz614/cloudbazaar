package com.app.ecommerce.eapp.Notification;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.view.MenuItem;

import com.app.ecommerce.eapp.R;

/**
 * Created by RFI on 28-10-2017.
 */

public class NotificationCountSetClass extends Activity {
    private static LayerDrawable icon;
    BadgeDrawable badge;

    public NotificationCountSetClass() {
    }

    public void setAddToCart(Context context, MenuItem item, int numMessages) {
        icon = (LayerDrawable) item.getIcon();

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(numMessages);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }
}
