package com.app.ecommerce.eapp.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RFI on 11-01-2018.
 */

public class ProductDetailsModel {
    @SerializedName("DefaultPictureZoomEnabled")
    public boolean DefaultPictureZoomEnabled;
    public boolean DefaultPictureZoomEnabled() { return DefaultPictureZoomEnabled; }

    @SerializedName("DefaultPictureModel")
    public PictureModel DefaultPictureModel;
    public PictureModel DefaultPictureModel() { return DefaultPictureModel; }

    @SerializedName("PictureModels")
    public List<PictureModel> PictureModels;
    public List<PictureModel> PictureModels() { return PictureModels; }
}