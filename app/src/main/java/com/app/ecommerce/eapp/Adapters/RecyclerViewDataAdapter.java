package com.app.ecommerce.eapp.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.app.ecommerce.eapp.Models.CategoriesProductsViewModel;
import com.app.ecommerce.eapp.Models.CategoryandProductViewmodel;
import com.app.ecommerce.eapp.R;

import java.util.List;

public class RecyclerViewDataAdapter extends RecyclerView.Adapter<RecyclerViewDataAdapter.ItemRowHolder> {

    private List<CategoryandProductViewmodel> dataList;
    private Context mContext;
    private onbtnMoreSelected mListener;

    public RecyclerViewDataAdapter(Context context, List<CategoryandProductViewmodel> dataList) {
        this.dataList = dataList;
        this.mContext = context;
        if (context instanceof onbtnMoreSelected) {
            mListener = (onbtnMoreSelected) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement OnRageComicSelected.");
        }
    }

    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView itemTitle;
        protected RecyclerView recycler_view_list;
        protected Button btnMore;

        public ItemRowHolder(View view) {
            super(view);
            this.itemTitle = (TextView) view.findViewById(R.id.itemTitle);
            this.recycler_view_list = (RecyclerView) view.findViewById(R.id.recycler_view_list);
            this.btnMore = (Button) view.findViewById(R.id.btnMore);
        }
    }

    @Override
    public void onBindViewHolder(ItemRowHolder itemRowHolder, int i) {

        final String categoryName = dataList.get(i).CategoryName();
        final String categoryId = Integer.toString(dataList.get(i).CategoryId());
        List<CategoriesProductsViewModel> categoriesProductsViewModels = dataList.get(i).ProductList();
        itemRowHolder.itemTitle.setText(categoryName);
        itemRowHolder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onbtnMoreSelected(v.getContext(), categoryId);
            }
        });

        SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(mContext, categoriesProductsViewModels);
        itemRowHolder.recycler_view_list.setHasFixedSize(true);
        itemRowHolder.recycler_view_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        itemRowHolder.recycler_view_list.setAdapter(itemListDataAdapter);
        itemRowHolder.recycler_view_list.setNestedScrollingEnabled(false);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
    }

    public interface onbtnMoreSelected {
        void onbtnMoreSelected(Context context, String categoryId);
    }
}