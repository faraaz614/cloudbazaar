package com.app.ecommerce.eapp.Fragments;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.ecommerce.eapp.Adapters.SectionListDataAdapter;
import com.app.ecommerce.eapp.Helpers.CategoryService;
import com.app.ecommerce.eapp.Helpers.LocalService;
import com.app.ecommerce.eapp.Helpers.ShoppingCartService;
import com.app.ecommerce.eapp.Helpers.Utility;
import com.app.ecommerce.eapp.MainActivity;
import com.app.ecommerce.eapp.Models.CategoriesProductsViewModel;
import com.app.ecommerce.eapp.Models.PictureModel;
import com.app.ecommerce.eapp.Models.ProductDetailResponse;
import com.app.ecommerce.eapp.Models.ProductViewModel;
import com.app.ecommerce.eapp.Models.ShoppingCartResponse;
import com.app.ecommerce.eapp.Models.UserModel;
import com.app.ecommerce.eapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.DrawableBanner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.views.BannerSlider;

public class RageComicDetailsFragment extends BaseFragment {

    private FragmentActivity myContext;
    public CategoriesProductsViewModel productViewModel;
    private static Retrofit retrofit = null;
    public static UserModel userModel = null;

    @Override
    public void onAttach(Activity activity) {
        myContext = (FragmentActivity) activity;
        super.onAttach(activity);
    }

    public static RageComicDetailsFragment newInstance() {
        final RageComicDetailsFragment fragment = new RageComicDetailsFragment();
        return fragment;
    }

    public void SetResult(CategoriesProductsViewModel productVM) {
        productViewModel = productVM;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_rage_comic_details, container, false);
        final TextView productidTextView = (TextView) view.findViewById(R.id.productid);
        final TextView nameTextView = (TextView) view.findViewById(R.id.name);
        final TextView priceTextView = (TextView) view.findViewById(R.id.price);
        final TextView fulldescriptionTextView = (TextView) view.findViewById(R.id.description);
        final ImageView increase = (ImageView) view.findViewById(R.id.increase);
        final ImageView decrease = (ImageView) view.findViewById(R.id.decrease);
        final TextView quantity = (TextView) view.findViewById(R.id.quantity);
        final RecyclerView recycler_view_list = (RecyclerView) view.findViewById(R.id.relatedproducts);
        final LinearLayout relatedlayout = (LinearLayout) view.findViewById(R.id.relatedlayout);
        final Button addtocart = (Button) view.findViewById(R.id.addcart);
        final Button addwishlist = (Button) view.findViewById(R.id.addwishlist);

        userModel = new LocalService(myContext).GetUser();

        productidTextView.setText(Integer.toString(productViewModel.ProductId()));
        nameTextView.setText(productViewModel.ProductName());
        priceTextView.setText(Utility.dubaicurrency + productViewModel.Price());
        String description = productViewModel.FullDescription();
        if (description != null) {
            fulldescriptionTextView.setText(Html.fromHtml(description));
        }

        increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int txtquantity = Integer.parseInt(quantity.getText().toString());
                txtquantity = txtquantity + 1;
                quantity.setText(Integer.toString(txtquantity));
                /*AddToCartWishlist(Integer.parseInt(productidTextView.getText().toString()),
                        Utility.shoppingCartTypeIdCart, txtquantity, userModel.getUserID());
                MainActivity.notificationCountCart++;*/
            }
        });

        decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int txtquantity = Integer.parseInt(quantity.getText().toString());
                txtquantity = txtquantity - 1;
                if (txtquantity <= 0) {
                    txtquantity = 1;
                }
                quantity.setText(Integer.toString(txtquantity));
                /*if (txtquantity >= 1) {
                    AddToCartWishlist(Integer.parseInt(productidTextView.getText().toString()),
                            Utility.shoppingCartTypeIdCart, txtquantity, userModel.getUserID());
                    MainActivity.notificationCountCart--;
                }*/
            }
        });

        BannerSlider bannerSlider = (BannerSlider) view.findViewById(R.id.detailbanner);
        List<Banner> banners = new ArrayList<>();
        for (PictureModel pic : productViewModel.PictureModel().PictureModels()) {
            banners.add(new RemoteBanner("http://www.solvemax.in/" + pic.FullSizeImageUrl()));
        }
        bannerSlider.setBanners(banners);

        List<CategoriesProductsViewModel> categoriesProductsViewModels = new ArrayList<>();
        categoriesProductsViewModels = productViewModel.RelateProducts();
        if (categoriesProductsViewModels.size() > 0) {
            relatedlayout.setVisibility(View.VISIBLE);
            SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(myContext, categoriesProductsViewModels);
            recycler_view_list.setHasFixedSize(true);
            recycler_view_list.setLayoutManager(new LinearLayoutManager(myContext, LinearLayoutManager.HORIZONTAL, false));
            recycler_view_list.setAdapter(itemListDataAdapter);
            recycler_view_list.setNestedScrollingEnabled(false);
        } else {
            relatedlayout.setVisibility(View.GONE);
        }

        addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userModel == null) {
                    add(new LoginFragment());
                } else {
                    AddToCartWishlist(Integer.parseInt(productidTextView.getText().toString()),
                            Utility.shoppingCartTypeIdCart,
                            Integer.parseInt(quantity.getText().toString()),
                            userModel.getUserID());
                    MainActivity.notificationCountCart++;
                }

            }
        });

        addwishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userModel == null) {
                    add(new LoginFragment());
                } else {
                    AddToCartWishlist(Integer.parseInt(productidTextView.getText().toString()),
                            Utility.shoppingCartTypeIdWish,
                            Integer.parseInt(quantity.getText().toString()),
                            userModel.getUserID());
                }
            }
        });

        return view;
    }

    @Override
    protected String getTitle() {
        return productViewModel.ProductName();
    }

    public void AddToCartWishlist(int productId, final int shoppingCartTypeId, int quantity, int CustomerID) {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        ShoppingCartService shoppingCartService = retrofit.create(ShoppingCartService.class);
        Call<ResponseBody> call = shoppingCartService.AddToCart(productId, shoppingCartTypeId, quantity, CustomerID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null) {
                    if (response.body().toString() != "") {

                    } else {

                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }
}
