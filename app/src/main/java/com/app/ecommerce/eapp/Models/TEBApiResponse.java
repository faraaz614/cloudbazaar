package com.app.ecommerce.eapp.Models;

import android.graphics.Movie;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RFI on 23-10-2017.
 */

public class TEBApiResponse {

    @SerializedName("IsSuccess")
    private boolean IsSuccess;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<CategoryandProductViewmodel> Data;

    public boolean IsSuccess() {
        return IsSuccess;
    }

    public String Message() {
        return Message;
    }

    public List<CategoryandProductViewmodel> Data() {
        return Data;
    }
}
