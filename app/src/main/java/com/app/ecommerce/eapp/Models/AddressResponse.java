package com.app.ecommerce.eapp.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RFI on 08-11-2017.
 */

public class AddressResponse {
    @SerializedName("IsSuccess")
    private boolean IsSuccess;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<AddressViewModel> Data;

    public boolean IsSuccess() {
        return IsSuccess;
    }

    public String Message() {
        return Message;
    }

    public List<AddressViewModel> Data() {
        return Data;
    }
}
