package com.app.ecommerce.eapp.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RFI on 08-11-2017.
 */

public class AddressViewModel {
    @SerializedName("Id")
    private int Id;
    public int Id() { return Id; }
    public void SetId(int Id) { Id = Id; }

    @SerializedName("FirstName")
    private String FirstName;
    public String FirstName() { return FirstName; }
    public void SetFirstName(String FirstName) { FirstName = FirstName; }

    @SerializedName("LastName")
    private String LastName;
    public String LastName() { return LastName; }
    public void SetLastName(String LastName) { LastName = LastName; }

    @SerializedName("Email")
    private String Email;
    public String Email() { return Email; }
    public void SetEmail(String Email) { Email = Email; }

    @SerializedName("Company")
    private String Company;
    public String Company() { return Company; }
    public void SetCompany(String Company) { Company = Company; }

    @SerializedName("Country")
    private String Country;
    public String Country() { return Country; }
    public void SetCountry(String Country) { Country = Country; }

    @SerializedName("CountryId")
    private int CountryId;
    public int CountryId() { return CountryId; }
    public void SetCountryId(int CountryId) { CountryId = CountryId; }

    @SerializedName("State")
    private String State;
    public String State() { return State; }
    public void SetState(String State) { State = State; }

    @SerializedName("StateprovinceId")
    private int StateprovinceId;
    public int StateprovinceId() { return StateprovinceId; }
    public void SetStateprovinceId(int StateprovinceId) { StateprovinceId = StateprovinceId; }

    @SerializedName("City")
    private String City;
    public String City() { return City; }
    public void SetCity(String City) { City = City; }

    @SerializedName("Address1")
    private String Address1;
    public String Address1() { return Address1; }
    public void SetAddress1(String Address1) { Address1 = Address1; }

    @SerializedName("Address2")
    private String Address2;
    public String Address2() { return Address2; }
    public void SetAddress2(String Address2) { Address2 = Address2; }

    @SerializedName("ZipPostalCode")
    private String ZipPostalCode;
    public String ZipPostalCode() { return ZipPostalCode; }
    public void SetZipPostalCode(String ZipPostalCode) { ZipPostalCode = ZipPostalCode; }

    @SerializedName("PhoneNumber")
    private String PhoneNumber;
    public String PhoneNumber() { return PhoneNumber; }
    public void SetPhoneNumber(String PhoneNumber) { PhoneNumber = PhoneNumber; }

    @SerializedName("FaxNumber")
    private String FaxNumber;
    public String FaxNumber() { return FaxNumber; }
    public void SetFaxNumber(String FaxNumber) { FaxNumber = FaxNumber; }

    @SerializedName("CustomerID")
    private int CustomerID;
    public int CustomerID() { return CustomerID; }
    public void SetCustomerID(int CustomerID) { CustomerID = CustomerID; }
}
