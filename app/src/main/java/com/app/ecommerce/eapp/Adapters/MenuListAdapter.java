package com.app.ecommerce.eapp.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ecommerce.eapp.Models.CategoryandProductViewmodel;
import com.app.ecommerce.eapp.Models.ChildCategory;
import com.app.ecommerce.eapp.R;

import java.util.List;

/**
 * Created by RFI on 24-10-2017.
 */

public class MenuListAdapter extends BaseExpandableListAdapter {
    private Activity context;
    private List<CategoryandProductViewmodel> _categories;

    public MenuListAdapter(Activity context, List<CategoryandProductViewmodel> categories) {
        this.context = context;
        this._categories = categories;
    }

    public Object getChild(int groupPosition, int childPosition) {
        Object coll = _categories.get(groupPosition).ChildCategory();
        if (coll != null) {
            return _categories.get(groupPosition).ChildCategory().get(childPosition);
        }
        return null;
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    public int getChildrenCount(int groupPosition) {
        List<ChildCategory> children = _categories.get(groupPosition).ChildCategory();
        if (children == null) {
            return 0;
        }
        return children.size();
    }

    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.menu_child_item, null);
        }

        final ChildCategory ccategory = (ChildCategory) getChild(groupPosition, childPosition);
        if (ccategory != null) {
            TextView childcategory = (TextView) convertView.findViewById(R.id.childcategory);
            TextView childcategoryid = (TextView) convertView.findViewById(R.id.childcategoryid);
            childcategory.setText(ccategory.CategoryName());
            childcategoryid.setText(Integer.toString(ccategory.CategoryId()));
        }
        return convertView;
    }

    public Object getGroup(int groupPosition) {
        return _categories.get(groupPosition);
    }

    public int getGroupCount() {
        return _categories.size();
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.menu_group_item,
                    null);
        }

        ImageView img = (ImageView) convertView.findViewById(R.id.arrow);
        if (isExpanded) {
            img.setImageResource(R.drawable.up);
        } else {
            img.setImageResource(R.drawable.down);
        }

        if (getChildrenCount(groupPosition) == 0) {
            img.setVisibility(View.GONE);
        }

        CategoryandProductViewmodel pcategory = (CategoryandProductViewmodel) getGroup(groupPosition);
        if (pcategory != null) {
            TextView parentcategory = (TextView) convertView.findViewById(R.id.parentcategory);
            TextView parentcategoryid = (TextView) convertView.findViewById(R.id.parentcategoryid);
            parentcategory.setText(pcategory.CategoryName());
            parentcategoryid.setText(Integer.toString(pcategory.CategoryId()));
        }
        return convertView;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}