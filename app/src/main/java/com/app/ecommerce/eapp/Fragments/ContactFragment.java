package com.app.ecommerce.eapp.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.ecommerce.eapp.R;

public class ContactFragment extends BaseFragment {
    private static View view;

    @Override
    protected String getTitle() {
        return "Contact";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.contact, container, false);
        return view;
    }
}
