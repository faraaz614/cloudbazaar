package com.app.ecommerce.eapp.Fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.ecommerce.eapp.Helpers.Utility;
import com.app.ecommerce.eapp.Models.ProductDetailsModel;
import com.app.ecommerce.eapp.Models.ProductViewModel;
import com.app.ecommerce.eapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Retrofit;

public class RageComicListFragment extends BaseFragment {
    private static Retrofit retrofit = null;
    private OnRageComicSelected mListener;
    public List<ProductViewModel> productViewModels;
    LinearLayout layoutCartItems;
    LinearLayout layout_norecords;
    Button bStartShopping;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnRageComicSelected) {
            mListener = (OnRageComicSelected) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement OnRageComicSelected.");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        final View view = inflater.inflate(R.layout.fragment_rage_comic_list, container, false);

        layoutCartItems = (LinearLayout) view.findViewById(R.id.layout_items);
        layout_norecords = (LinearLayout) view.findViewById(R.id.layout_norecords);

        if (productViewModels.size() > 0) {
            layout_norecords.setVisibility(View.GONE);
            layoutCartItems.setVisibility(View.VISIBLE);
        } else {
            layout_norecords.setVisibility(View.VISIBLE);
            layoutCartItems.setVisibility(View.GONE);

            bStartShopping = (Button) view.findViewById(R.id.bAddNew);
            bStartShopping.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        getActivity().getSupportFragmentManager().popBackStackImmediate();
                    }
                }
            });
        }

        final Activity activity = getActivity();
        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(activity, 2));
        recyclerView.setAdapter(new RageComicAdapter(activity));
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        MenuItem m = menu.findItem(R.id.filter);
        if (m != null) {
            m.setVisible(true);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    protected String getTitle() {
        return " ";
    }

    public interface OnRageComicSelected {
        void onRageComicSelected(int productId);
    }

    public static RageComicListFragment newInstance() {
        return new RageComicListFragment();
    }

    public void SetResult(List<ProductViewModel> productVM) {
        productViewModels = productVM;
    }

    class RageComicAdapter extends RecyclerView.Adapter<RageComicAdapter.ViewHolder> {

        private LayoutInflater mLayoutInflater;

        public RageComicAdapter(Context context) {
            mLayoutInflater = LayoutInflater.from(context);
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView mImageView;
            private TextView mNameTextView;
            private TextView mprice;

            private ViewHolder(View itemView) {
                super(itemView);
                mImageView = (ImageView) itemView.findViewById(R.id.itemImage);
                mNameTextView = (TextView) itemView.findViewById(R.id.itemTitle);
                mprice = (TextView) itemView.findViewById(R.id.itemPrice);
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            return new ViewHolder(mLayoutInflater
                    .inflate(R.layout.recycler_item_rage_comic, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, final int position) {
            final ProductViewModel model = productViewModels.get(position);
            if (model != null) {
                final String name = model.ProductName();
                final String url = model.PictureModel().DefaultPictureModel().FullSizeImageUrl();
                final String price = Utility.dubaicurrency + Double.toString(model.Price());
                viewHolder.mNameTextView.setText(name);
                viewHolder.mprice.setText(price);

                String urll = "";
                if (model != null && model.PictureModel() != null
                        && model.PictureModel().DefaultPictureModel() != null
                        && !model.PictureModel().DefaultPictureModel().FullSizeImageUrl().isEmpty()){
                    urll = "http://www.solvemax.in/" + model.PictureModel().DefaultPictureModel().FullSizeImageUrl();
                    Picasso.with(getContext()).load(urll)
                            //.placeholder(R.drawable.android).error(R.drawable.android)
                            .into(viewHolder.mImageView);
                }

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.onRageComicSelected(model.ProductId());
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return productViewModels.size();
        }
    }
}
