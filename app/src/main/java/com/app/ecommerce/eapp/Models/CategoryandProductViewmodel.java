package com.app.ecommerce.eapp.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RFI on 24-10-2017.
 */

public class CategoryandProductViewmodel {
    @SerializedName("CategoryId")
    private int CategoryId;
    @SerializedName("CategoryName")
    private String CategoryName;
    @SerializedName("ParentCategoryId")
    private int ParentCategoryId;
    @SerializedName("ChildCategory")
    private List<ChildCategory> ChildCategory;
    @SerializedName("ProductList")
    private List<CategoriesProductsViewModel> ProductList;

    public int CategoryId() {
        return CategoryId;
    }

    public String CategoryName() {
        return CategoryName;
    }

    public int ParentCategoryId() {
        return ParentCategoryId;
    }

    public List<ChildCategory> ChildCategory() { return ChildCategory; }

    public List<CategoriesProductsViewModel> ProductList() { return ProductList; }
}
