package com.app.ecommerce.eapp;

import android.app.ActionBar;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SearchRecentSuggestionsProvider;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.ecommerce.eapp.Fragments.AccountFragment;
import com.app.ecommerce.eapp.Fragments.LoginFragment;
import com.app.ecommerce.eapp.Fragments.RageComicDetailsFragment;
import com.app.ecommerce.eapp.Fragments.RageComicListFragment;
import com.app.ecommerce.eapp.Helpers.CategoryService;
import com.app.ecommerce.eapp.Helpers.LocalService;
import com.app.ecommerce.eapp.Helpers.Utility;
import com.app.ecommerce.eapp.Models.CategoriesProductsViewModel;
import com.app.ecommerce.eapp.Models.CategoryProductResponse;
import com.app.ecommerce.eapp.Models.ProductDetailResponse;
import com.app.ecommerce.eapp.Models.ProductViewModel;
import com.app.ecommerce.eapp.Notification.NotificationCountSetClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by RFI on 06-01-2018.
 */

public class SearchActivity extends BaseActivity implements
                RageComicListFragment.OnRageComicSelected,
                NavigationView.OnNavigationItemSelectedListener {

    private static Retrofit retrofit = null;
    public List<ProductViewModel> productViewModels;
    public CategoriesProductsViewModel productViewModel;
    private ActionBarDrawerToggle toggle;
    private DrawerLayout drawer;
    public AlertDialog levelDialog;
    final CharSequence[] items = {" Name: A to Z "," Name : Z to A"," Price : Low to High"," Price : High to Low"};
    public String query = "";
    private ProgressBar pgsBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);

        pgsBar = (ProgressBar) findViewById(R.id.pBar);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        /*drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, 0, 0);
        drawer.addDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.syncState();*/

        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        EditText searchtxt = (EditText)findViewById(R.id.searchtxt);
        searchtxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    SearchRecentSuggestions suggestions = new SearchRecentSuggestions(getApplicationContext(),
                            CBSuggestionProvider.AUTHORITY, CBSuggestionProvider.MODE);
                    suggestions.saveRecentQuery(query, null);

                    GetCategoryProduct(v.getText().toString(), "");
                    ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((getWindow().getDecorView().getApplicationWindowToken()), 0);
                    return true;
                }
                return false;
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String query = extras.getString(Utility.searchtxt);
            if (query != null) {
                searchtxt.setText(query);
                GetCategoryProduct(query, "");
            }
        }

        /*Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            query = intent.getStringExtra(SearchManager.QUERY);
            GetCategoryProduct(query, "");
        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            String uri = intent.getDataString();
        }*/
    }

    @Override
    protected ActionBarDrawerToggle getDrawerToggle() {
        return toggle;
    }

    @Override
    protected DrawerLayout getDrawer() {
        return drawer;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        EditText searchtxt = (EditText)findViewById(R.id.searchtxt);
        query = searchtxt.getText().toString();
        int id = item.getItemId();
        if (id == R.id.search){
            onSearchRequested();
        } else if (id == R.id.filter) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Sort By");
            builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    switch(item)
                    {
                        case 0:
                            GetCategoryProduct(query, "A to Z");
                            break;
                        case 1:
                            GetCategoryProduct(query, "Z to A");
                            break;
                        case 2:
                            GetCategoryProduct(query, "Low to High");
                            break;
                        case 3:
                            GetCategoryProduct(query, "High to Low");
                            break;

                    }
                    levelDialog.dismiss();
                }
            });
            levelDialog = builder.create();
            levelDialog.show();
        } else if (id == R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.search);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo( searchManager.getSearchableInfo(getComponentName()) );

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        invalidateOptionsMenu();
        return super.onPrepareOptionsMenu(menu);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onRageComicSelected(int productId) {
        GetProductDetailsByID(productId);
    }

    public void GetCategoryProduct(String query, String sort) {

        pgsBar.setVisibility(View.VISIBLE);

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        CategoryService categoryService = retrofit.create(CategoryService.class);
        Call<CategoryProductResponse> call = categoryService.GetProductByCategoryID(0, 1, query, sort);
        call.enqueue(new Callback<CategoryProductResponse>() {
            @Override
            public void onResponse(Call<CategoryProductResponse> call, Response<CategoryProductResponse> response) {
                if (response.body() != null && response.body().Data() != null) {
                    productViewModels = response.body().Data();
                    RageComicListFragment rageComicListFragment = new RageComicListFragment();
                    rageComicListFragment.SetResult(productViewModels);
                    add(rageComicListFragment);
                    pgsBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<CategoryProductResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }

    public void GetProductDetailsByID(int ID) {

        pgsBar.setVisibility(View.VISIBLE);

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        CategoryService categoryService = retrofit.create(CategoryService.class);
        Call<ProductDetailResponse> call = categoryService.GetProductDetailsByID(ID);
        call.enqueue(new Callback<ProductDetailResponse>() {
            @Override
            public void onResponse(Call<ProductDetailResponse> call, Response<ProductDetailResponse> response) {
                if (response.body() != null && response.body().Data() != null) {
                    productViewModel = response.body().Data();
                    final RageComicDetailsFragment detailsFragment = new RageComicDetailsFragment();
                    detailsFragment.SetResult(productViewModel);
                    add(detailsFragment);
                    pgsBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ProductDetailResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }
}
