package com.app.ecommerce.eapp.Helpers;

import com.app.ecommerce.eapp.Models.ClientToken;
import com.app.ecommerce.eapp.Models.ShoppingCartResponse;
import com.app.ecommerce.eapp.Models.Transaction;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by RFI on 22-07-2018.
 */

public interface PaymentClientApi {

    @GET("api/Payment/GenerateClientToken")
    Call<ClientToken> getClientToken(@Query("aCustomerId") String customerId, @Query("aMerchantAccountid") String merchantAccountId);

    @FormUrlEncoded
    @POST("api/Payment/CreatePurchase")
    Call<Transaction> createTransaction(@Field("nonce") String nonce, @Field("merchant_account_id") String merchantAccountId);
}
