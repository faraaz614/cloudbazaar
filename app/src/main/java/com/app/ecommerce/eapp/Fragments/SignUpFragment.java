package com.app.ecommerce.eapp.Fragments;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.ecommerce.eapp.Helpers.LocalService;
import com.app.ecommerce.eapp.Helpers.LoginService;
import com.app.ecommerce.eapp.Helpers.Utility;
import com.app.ecommerce.eapp.MainActivity;
import com.app.ecommerce.eapp.Models.Customer;
import com.app.ecommerce.eapp.Models.CustomerResponse;
import com.app.ecommerce.eapp.Models.UserModel;
import com.app.ecommerce.eapp.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by RFI on 17-10-2017.
 */

public class SignUpFragment extends BaseFragment implements View.OnClickListener {
    private static Retrofit retrofit = null;
    private static View view;
    private static EditText fullName, emailId, password, confirmPassword;
    private static TextView login, lblmsg;
    private static Button signUpButton;
    private static CheckBox terms_conditions;
    public static final String regEx = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b";

    public SignUpFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.signup_layout, container, false);
        fullName = (EditText) view.findViewById(R.id.fullName);
        emailId = (EditText) view.findViewById(R.id.userEmailId);
        password = (EditText) view.findViewById(R.id.password);
        confirmPassword = (EditText) view.findViewById(R.id.confirmPassword);
        signUpButton = (Button) view.findViewById(R.id.signUpBtn);
        login = (TextView) view.findViewById(R.id.already_user);
        terms_conditions = (CheckBox) view.findViewById(R.id.terms_conditions);
        lblmsg = (TextView) view.findViewById(R.id.lblmsg);

        signUpButton.setOnClickListener(this);
        login.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signUpBtn:
                checkValidation();
                break;
            case R.id.already_user:
                add(new LoginFragment());
                break;
        }
    }

    @Override
    protected String getTitle() {
        return "Sign Up";
    }

    private void checkValidation() {
        String getFullName = fullName.getText().toString();
        String getEmailId = emailId.getText().toString();
        String getPassword = password.getText().toString();
        String getConfirmPassword = confirmPassword.getText().toString();

        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(getEmailId);

        if (getFullName.equals("") || getFullName.length() == 0
                || getEmailId.equals("") || getEmailId.length() == 0
                || getPassword.equals("") || getPassword.length() == 0
                || getConfirmPassword.equals("")
                || getConfirmPassword.length() == 0)

            lblmsg.setText("All fields are required.");
        else if (!m.find()) {
            lblmsg.setText("Your Email Id is Invalid.");
        } else if (!getConfirmPassword.equals(getPassword)) {
            lblmsg.setText("Both password doesn't match.");
        } else if (!terms_conditions.isChecked()) {
            lblmsg.setText("Please select Terms and Conditions.");
        } else {
            ApiRegister(getEmailId, getPassword, getFullName);
        }
    }

    public void ApiRegister(String getEmailId, String getPassword, String getUsername) {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        LoginService loginService = retrofit.create(LoginService.class);
        Call<CustomerResponse> call = loginService.PostCustomer(getUsername, getPassword, getEmailId);
        call.enqueue(new Callback<CustomerResponse>() {
            @Override
            public void onResponse(Call<CustomerResponse> call, Response<CustomerResponse> response) {
                CustomerResponse customer = response.body();
                if (customer != null && customer.Data() != null) {
                    add(new LoginFragment());
                } else {
                    lblmsg.setText("Invalid details.");
                }
            }

            @Override
            public void onFailure(Call<CustomerResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }
}
