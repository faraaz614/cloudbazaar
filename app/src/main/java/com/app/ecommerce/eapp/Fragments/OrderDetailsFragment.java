package com.app.ecommerce.eapp.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.ecommerce.eapp.Adapters.SectionListDataAdapter;
import com.app.ecommerce.eapp.Helpers.LocalService;
import com.app.ecommerce.eapp.MainActivity;
import com.app.ecommerce.eapp.Models.CategoriesProductsViewModel;
import com.app.ecommerce.eapp.Models.OrderViewModel;
import com.app.ecommerce.eapp.Models.ShoppingCartItemViewModel;
import com.app.ecommerce.eapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RFI on 01-11-2017.
 */

public class OrderDetailsFragment extends BaseFragment {

    private FragmentActivity myContext;
    public OrderViewModel orderViewModel;

    @Override
    public void onAttach(Activity activity) {
        myContext = (FragmentActivity) activity;
        super.onAttach(activity);
    }

    public void SetResult(OrderViewModel orderVM) {
        orderViewModel = orderVM;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.orders_details, container, false);

        TextView orderno = (TextView) view.findViewById(R.id.orderno);
        TextView orderdate = (TextView) view.findViewById(R.id.orderdate);
        TextView email = (TextView) view.findViewById(R.id.email);
        TextView mobile = (TextView) view.findViewById(R.id.mobile);
        TextView address = (TextView) view.findViewById(R.id.address);
        TextView csc = (TextView) view.findViewById(R.id.csc);
        TextView paymentmethod = (TextView) view.findViewById(R.id.paymentmethod);
        TextView paymentstatus = (TextView) view.findViewById(R.id.paymentstatus);
        TextView shippingmethod = (TextView) view.findViewById(R.id.shippingmethod);
        TextView shippingstatus = (TextView) view.findViewById(R.id.shippingstatus);
        TextView subtotal = (TextView) view.findViewById(R.id.subtotal);
        TextView shipping = (TextView) view.findViewById(R.id.shipping);
        TextView tax = (TextView) view.findViewById(R.id.tax);
        TextView discount = (TextView) view.findViewById(R.id.discount);
        TextView total = (TextView) view.findViewById(R.id.total);

        orderno.setText("Order no : " + Integer.toString(orderViewModel.OrderID()));
        orderdate.setText("Order date : " +orderViewModel.OrderDate());
        email.setText("Order status : " +orderViewModel.OrderStatus());
        mobile.setText("Order price : " +orderViewModel.OrderPrice());
        if(orderViewModel.Address() != "null" || !orderViewModel.Address().equals(null)){
            address.setText(orderViewModel.Address());
        }
        if(orderViewModel.City() != "null" || !orderViewModel.City().equals(null)
                || orderViewModel.ZipCode() != "null" || !orderViewModel.ZipCode().equals(null)){
            csc.setText(orderViewModel.City() + "" + orderViewModel.ZipCode());
        }
        paymentmethod.setText(orderViewModel.PaymentMethod());
        paymentstatus.setText(orderViewModel.PaymentStatus());
        shippingmethod.setText(orderViewModel.ShipmentMethod());
        shippingstatus.setText(orderViewModel.ShipmentStatus());
        subtotal.setText("");
        shipping.setText("");
        tax.setText("");
        discount.setText("");
        total.setText("");

        List<CategoriesProductsViewModel> list = new ArrayList<>();
        for (ShoppingCartItemViewModel prod : orderViewModel.ProductsList){
            CategoriesProductsViewModel model= new CategoriesProductsViewModel();
            model.SetProductId(prod.ProductId());
            model.SetProductName(prod.ProductName());
            model.SetPrice(prod.Price());
            model.SetPicBinary(prod.PictureBinary());
            model.SetProductDetailsModel(prod.PictureModel());
            list.add(model);
        }

        final RecyclerView recycler_view_list = (RecyclerView) view.findViewById(R.id.productlist);
        SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(myContext, list);
        recycler_view_list.setHasFixedSize(true);
        recycler_view_list.setLayoutManager(new LinearLayoutManager(myContext, LinearLayoutManager.HORIZONTAL, false));
        recycler_view_list.setAdapter(itemListDataAdapter);
        recycler_view_list.setNestedScrollingEnabled(false);

        return view;
    }

    @Override
    protected String getTitle() {
        return "Order Details";
    }
}

