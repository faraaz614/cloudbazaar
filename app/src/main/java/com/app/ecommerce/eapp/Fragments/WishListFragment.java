package com.app.ecommerce.eapp.Fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.ecommerce.eapp.Helpers.LocalService;
import com.app.ecommerce.eapp.Helpers.ShoppingCartService;
import com.app.ecommerce.eapp.Helpers.Utility;
import com.app.ecommerce.eapp.Models.CategoriesProductsViewModel;
import com.app.ecommerce.eapp.Models.ShoppingCartItemViewModel;
import com.app.ecommerce.eapp.Models.ShoppingCartResponse;
import com.app.ecommerce.eapp.Models.UserModel;
import com.app.ecommerce.eapp.R;
import com.squareup.picasso.Picasso;

import java.net.UnknownServiceException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by RFI on 30-10-2017.
 */

public class WishListFragment extends BaseFragment {
    private static View view;
    public List<ShoppingCartItemViewModel> listWishListModel;
    public UserModel userModel = new UserModel();
    private static Retrofit retrofit = null;

    public RageComicListFragment.OnRageComicSelected mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof RageComicListFragment.OnRageComicSelected) {
            mListener = (RageComicListFragment.OnRageComicSelected) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement OnRageComicSelected.");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.wishlist, container, false);
        userModel = new LocalService(getContext()).GetUser();
        TextView payment = (TextView) view.findViewById(R.id.text_action_bottom2);
        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (ShoppingCartItemViewModel model : listWishListModel) {
                    new RageComicDetailsFragment().AddToCartWishlist(model.ProductId(),
                            Utility.shoppingCartTypeIdCart, 1, userModel.getUserID());
                }
            }
        });
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        RecyclerView.LayoutManager recylerViewLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(recylerViewLayoutManager);
        recyclerView.setAdapter(new WishListAdapter(view, listWishListModel));
        return view;
    }

    @Override
    protected String getTitle() {
        return "WishList";
    }

    public void SetResult(List<ShoppingCartItemViewModel> shoppingCartVM) {
        listWishListModel = shoppingCartVM;
    }

    public class WishListAdapter
            extends RecyclerView.Adapter<WishListAdapter.ViewHolder> {

        private List<ShoppingCartItemViewModel> mwishlist;
        private View mView;

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final ImageView image;
            public final TextView productid, productname, price;
            public final LinearLayout mLayoutDelete, mLayoutAdd;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                image = (ImageView) view.findViewById(R.id.image_wishlist);
                productid = (TextView) view.findViewById(R.id.productid);
                productname = (TextView) view.findViewById(R.id.name);
                price = (TextView) view.findViewById(R.id.price);
                mLayoutDelete = (LinearLayout) view.findViewById(R.id.layout_action1);
                mLayoutAdd = (LinearLayout) view.findViewById(R.id.layout_action2);
            }
        }

        public WishListAdapter(View view, List<ShoppingCartItemViewModel> wishlist) {
            mwishlist = wishlist;
            mView = view;
        }

        @Override
        public WishListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.wishlist_list, parent, false);
            return new WishListAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final WishListAdapter.ViewHolder holder, final int position) {
            final ShoppingCartItemViewModel model = mwishlist.get(position);
            if (model != null) {
                String urll = "";
                if (model != null && model.PictureModel() != null
                        && model.PictureModel().DefaultPictureModel() != null
                        && !model.PictureModel().DefaultPictureModel().FullSizeImageUrl().isEmpty()){
                    urll = "http://www.solvemax.in/" + model.PictureModel().DefaultPictureModel().FullSizeImageUrl();
                    Picasso.with(getContext()).load(urll)
                            //.placeholder(R.drawable.android).error(R.drawable.android)
                            .into(holder.image);
                }

                holder.productid.setText(Integer.toString(model.ProductId()));
                holder.productname.setText(model.ProductName());
                holder.price.setText(model.Price());

                holder.mLayoutDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DeleteFromWishList(model.ProductId(), Utility.shoppingCartTypeIdWish);
                        mwishlist.remove(position);
                        notifyDataSetChanged();
                    }
                });

                holder.mLayoutAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        userModel = new LocalService(getContext()).GetUser();
                        new RageComicDetailsFragment().AddToCartWishlist(model.ProductId(),
                                Utility.shoppingCartTypeIdCart, 1, userModel.getUserID());
                        mwishlist.remove(position);
                        notifyDataSetChanged();
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return mwishlist.size();
        }

        public void DeleteFromWishList(int ProductID, int shoppingCartTypeId) {

            if (retrofit == null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(Utility.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            boolean Removefromcart = true;
            int quantity = 1;
            UserModel userModel = new LocalService(getContext()).GetUser();
            ShoppingCartService shoppingCartService = retrofit.create(ShoppingCartService.class);
            Call<ShoppingCartResponse> call = shoppingCartService.UpdateShoppingCartApp(Integer.toString(ProductID),
                    shoppingCartTypeId,
                    quantity,
                    userModel.getUserID(),
                    Removefromcart);
            call.enqueue(new Callback<ShoppingCartResponse>() {
                @Override
                public void onResponse(Call<ShoppingCartResponse> call, Response<ShoppingCartResponse> response) {

                }

                @Override
                public void onFailure(Call<ShoppingCartResponse> call, Throwable throwable) {
                    Log.e("eApp", throwable.toString());
                }
            });
        }
    }
}
