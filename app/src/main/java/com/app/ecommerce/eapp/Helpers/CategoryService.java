package com.app.ecommerce.eapp.Helpers;

import com.app.ecommerce.eapp.Models.CategoryProductResponse;
import com.app.ecommerce.eapp.Models.ProductDetailResponse;
import com.app.ecommerce.eapp.Models.TEBApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by RFI on 23-10-2017.
 */

public interface CategoryService {
    @GET("api/Category/GetAllCategoryAndProducts")
    Call<TEBApiResponse> GetAllCategoryAndProducts();

    @GET("api/Category/GetProductByCategoryID")
    Call<CategoryProductResponse> GetProductByCategoryID(@Query("CategoryID") int CategoryID, @Query("Pageid") int Pageid);

    @GET("api/Category/GetProductByCategoryID")
    Call<CategoryProductResponse> GetProductByCategoryID(@Query("CategoryID") int CategoryID, @Query("Pageid") int Pageid,
                                                         @Query("Productsname") String Productsname, @Query("SortbyText") String SortbyText);

    @GET("api/Category/GetProductDetailsByID")
    Call<ProductDetailResponse> GetProductDetailsByID(@Query("ID") int ID);
}
