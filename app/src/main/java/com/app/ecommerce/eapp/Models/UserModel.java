package com.app.ecommerce.eapp.Models;

public class UserModel {
    public String name;
    public int userid;

    public UserModel() {
    }

    public UserModel(int userid, String name) {
        this.userid = userid;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getUserID() {
        return userid;
    }
}
