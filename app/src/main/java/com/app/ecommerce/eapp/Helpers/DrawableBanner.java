package com.app.ecommerce.eapp.Helpers;

import android.graphics.drawable.Drawable;
import android.os.Parcel;

import ss.com.bannerslider.banners.RemoteBanner;

/**
 * Created by RFI on 28-10-2017.
 */

public class DrawableBanner extends RemoteBanner {

    public DrawableBanner(Drawable drawable){
        super(String.valueOf(drawable));
        super.setPlaceHolder(drawable);
    }
}
