package com.app.ecommerce.eapp.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.Visibility;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.ecommerce.eapp.Helpers.LocalService;
import com.app.ecommerce.eapp.Helpers.PaymentClientApi;
import com.app.ecommerce.eapp.Helpers.ShoppingCartService;
import com.app.ecommerce.eapp.Helpers.Utility;
import com.app.ecommerce.eapp.MainActivity;
import com.app.ecommerce.eapp.Models.CategoriesProductsViewModel;
import com.app.ecommerce.eapp.Models.ClientToken;
import com.app.ecommerce.eapp.Models.OrderViewModel;
import com.app.ecommerce.eapp.Models.OrdersResponse;
import com.app.ecommerce.eapp.Models.ShoppingCartItemViewModel;
import com.app.ecommerce.eapp.Models.ShoppingCartResponse;
import com.app.ecommerce.eapp.Models.TEBApiResponse;
import com.app.ecommerce.eapp.Models.Transaction;
import com.app.ecommerce.eapp.Models.UserModel;
import com.app.ecommerce.eapp.R;
import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.PayPal;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.braintreepayments.api.interfaces.PaymentMethodNonceCreatedListener;
import com.braintreepayments.api.models.PayPalAccountNonce;
import com.braintreepayments.api.models.PayPalRequest;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.braintreepayments.api.models.PostalAddress;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by RFI on 27-10-2017.
 */

public class CartFragment extends BaseFragment implements PaymentMethodNonceCreatedListener {
    private static View view;
    public List<ShoppingCartItemViewModel> listCartModel;
    private static Retrofit retrofit = null;
    public List<OrderViewModel> orderViewModels;
    public RageComicListFragment.OnRageComicSelected mListener;
    LinearLayout layoutCartItems;
    LinearLayout layoutCartPayments;
    LinearLayout layoutCartNoItems;
    Button bStartShopping;
    protected String mAuthorization, mCustomerId, mMerchantAccountId;
    BraintreeFragment mBraintreeFragment;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof RageComicListFragment.OnRageComicSelected) {
            mListener = (RageComicListFragment.OnRageComicSelected) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement OnRageComicSelected.");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.cart, container, false);

        layoutCartItems = (LinearLayout) view.findViewById(R.id.layout_items);
        layoutCartPayments = (LinearLayout) view.findViewById(R.id.layout_payment);
        layoutCartNoItems = (LinearLayout) view.findViewById(R.id.layout_cart_empty);

        if (listCartModel.size() > 0) {
            layoutCartNoItems.setVisibility(View.GONE);
            layoutCartItems.setVisibility(View.VISIBLE);
            layoutCartPayments.setVisibility(View.VISIBLE);
        } else {
            layoutCartNoItems.setVisibility(View.VISIBLE);
            layoutCartItems.setVisibility(View.GONE);
            layoutCartPayments.setVisibility(View.GONE);

            bStartShopping = (Button) view.findViewById(R.id.bAddNew);
            bStartShopping.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        getActivity().getSupportFragmentManager().popBackStackImmediate();
                    }
                }
            });
        }
        //double carttotal = new LocalService(getContext()).CartTotal();
        TextView totaltxt = (TextView) view.findViewById(R.id.text_action_bottom1);
        //totaltxt.setText("$" + Double.toString(carttotal));
        TextView payment = (TextView) view.findViewById(R.id.text_action_bottom2);
        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlaceOrder();
            }
        });
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        RecyclerView.LayoutManager recylerViewLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(recylerViewLayoutManager);
        recyclerView.setAdapter(new CartAdapter(view, listCartModel));
        return view;
    }

    @Override
    protected String getTitle() {
        return "Shopping Cart";
    }

    public void SetResult(List<ShoppingCartItemViewModel> shoppingCartVM) {
        listCartModel = new ArrayList<>();
        listCartModel = shoppingCartVM;
    }

    public class CartAdapter
            extends RecyclerView.Adapter<CartAdapter.ViewHolder> {

        private List<ShoppingCartItemViewModel> mcartlist;
        private View mView;

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final ImageView mImageView;
            public final TextView mproductname, mproductprice, mproductdesc, mquantity, mproductid;
            public final LinearLayout mLayoutItem, mLayoutRemove, mLayoutEdit;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mImageView = (ImageView) view.findViewById(R.id.image_cartlist);
                mproductname = (TextView) view.findViewById(R.id.name);
                mproductprice = (TextView) view.findViewById(R.id.price);
                mproductdesc = (TextView) view.findViewById(R.id.desc);
                mquantity = (TextView) view.findViewById(R.id.quantity);
                mproductid = (TextView) view.findViewById(R.id.productid);
                mLayoutItem = (LinearLayout) view.findViewById(R.id.layoutItem);
                mLayoutRemove = (LinearLayout) view.findViewById(R.id.layout_action1);
                mLayoutEdit = (LinearLayout) view.findViewById(R.id.layout_action2);
            }
        }

        public CartAdapter(View view, List<ShoppingCartItemViewModel> cartlist) {
            mcartlist = cartlist;
            mView = view;
        }

        @Override
        public CartAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_list, parent, false);
            return new CartAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final CartAdapter.ViewHolder holder, final int position) {
            final ShoppingCartItemViewModel model = mcartlist.get(position);
            if (model != null) {
                String description = model.Description();
                if (model.Description() != null && model.Description().length() > 40) {
                    description = model.Description().substring(0, 40);
                }
                String urll = "";
                if (model != null && model.PictureModel() != null
                        && model.PictureModel().DefaultPictureModel() != null
                        && !model.PictureModel().DefaultPictureModel().FullSizeImageUrl().isEmpty()) {
                    urll = "http://www.solvemax.in/" + model.PictureModel().DefaultPictureModel().FullSizeImageUrl();
                    Picasso.with(getContext()).load(urll)
                            //.placeholder(R.drawable.android).error(R.drawable.android)
                            .into(holder.mImageView);
                }

                holder.mproductid.setText(Integer.toString(model.ProductId()));
                holder.mproductname.setText(model.ProductName());
                holder.mproductprice.setText(Utility.dubaicurrency + model.Price());
                holder.mproductdesc.setText(description);
                holder.mquantity.setText("Qty : " + model.Quantity());

                holder.mLayoutItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.onRageComicSelected(model.ProductId());
                    }
                });

                holder.mLayoutRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DeleteFromCart(model.ProductId(), Utility.shoppingCartTypeIdCart);
                        Iterator<ShoppingCartItemViewModel> item = mcartlist.iterator();

                        while (item.hasNext()) {
                            ShoppingCartItemViewModel str = item.next();
                            if (str.ProductId() == model.ProductId()) {
                                item.remove();
                            }
                        }

                        notifyDataSetChanged();
                        TextView totaltxt = (TextView) mView.findViewById(R.id.text_action_bottom1);
                        MainActivity.notificationCountCart = MainActivity.notificationCountCart - model.Quantity();
                        if (mcartlist.size() == 0) {
                            MainActivity.notificationCountCart = 0;
                            layoutCartNoItems.setVisibility(View.VISIBLE);
                            layoutCartItems.setVisibility(View.GONE);
                            layoutCartPayments.setVisibility(View.GONE);
                        }
                    }
                });

                holder.mLayoutEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return mcartlist.size();
        }
    }

    public void DeleteFromCart(int ProductID, int shoppingCartTypeId) {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        boolean Removefromcart = true;
        int quantity = 1;
        UserModel userModel = new LocalService(getContext()).GetUser();
        ShoppingCartService shoppingCartService = retrofit.create(ShoppingCartService.class);
        Call<ShoppingCartResponse> call = shoppingCartService.UpdateShoppingCartApp(Integer.toString(ProductID),
                shoppingCartTypeId,
                quantity,
                userModel.getUserID(),
                Removefromcart);
        call.enqueue(new Callback<ShoppingCartResponse>() {
            @Override
            public void onResponse(Call<ShoppingCartResponse> call, Response<ShoppingCartResponse> response) {
                if (response.body() != null && response.body().Data() != null) {
                    listCartModel = response.body().Data();
                }
            }

            @Override
            public void onFailure(Call<ShoppingCartResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }

    public void GetUserOrders() {
        orderViewModels = new ArrayList<>();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        UserModel userModel = new LocalService(getContext()).GetUser();
        ShoppingCartService shoppingCartService = retrofit.create(ShoppingCartService.class);
        Call<OrdersResponse> call = shoppingCartService.GetCustomerOrders(userModel.getUserID());
        call.enqueue(new Callback<OrdersResponse>() {
            @Override
            public void onResponse(Call<OrdersResponse> call, Response<OrdersResponse> response) {
                if (response.body() != null && response.body().Data() != null) {
                    orderViewModels = response.body().Data();
                    OrdersFragment ordersFragment = new OrdersFragment();
                    ordersFragment.SetResult(orderViewModels);
                    add(ordersFragment);
                }
            }

            @Override
            public void onFailure(Call<OrdersResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }

    public void PlaceOrder() {

        try {
            if (mAuthorization == null) {
                fetchAuthorization();
            }
            if (mBraintreeFragment == null) {
                mBraintreeFragment = BraintreeFragment.newInstance(getActivity(), mAuthorization);
            }
            PayPalRequest request = new PayPalRequest("1").currencyCode("USD").intent(PayPalRequest.INTENT_AUTHORIZE);
            PayPal.requestOneTimePayment(mBraintreeFragment, request);


        } catch (InvalidArgumentException e) {
            // There was an issue with your authorization string.
        }

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        UserModel userModel = new LocalService(getContext()).GetUser();
        ShoppingCartService shoppingCartService = retrofit.create(ShoppingCartService.class);
        Call<Void> call = shoppingCartService.placeOrder(userModel.getUserID());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
                    } else {
                        builder = new AlertDialog.Builder(getContext());
                    }
                    builder.setTitle("Order Confirm")
                            .setMessage("Your order has been placed.")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    GetUserOrders();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }

    @Override
    public void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethodNonce) {
        if (paymentMethodNonce instanceof PayPalAccountNonce) {
            PayPalAccountNonce payPalAccountNonce = (PayPalAccountNonce) paymentMethodNonce;
            String details = getDisplayString((PayPalAccountNonce) payPalAccountNonce);

            PaymentClientApi paymentClientApi = retrofit.create(PaymentClientApi.class);
            Call<Transaction> call = paymentClientApi.createTransaction(payPalAccountNonce.getNonce(), mMerchantAccountId);
            call.enqueue(new Callback<Transaction>() {

                @Override
                public void onResponse(Call<Transaction> call, Response<Transaction> response) {
                    if (response.isSuccessful()) {
                        Transaction transaction = response.body();
                        if (transaction.getMessage() != null &&
                                transaction.getMessage().startsWith("created")) {
                            //setStatus("transaction_complete");
                            //setMessage(transaction.getMessage());
                            AlertDialog.Builder builder;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
                            } else {
                                builder = new AlertDialog.Builder(getContext());
                            }
                            builder.setTitle("Order Confirm")
                                    .setMessage("Your order has been placed.")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            GetUserOrders();
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        } else {
                            String msg = "Transaction failed";
                            //setStatus("transaction_failed");
                            if (TextUtils.isEmpty(transaction.getMessage())) {
                                //setMessage("Server response was empty or malformed");
                                msg = "Server response was empty or malformed";
                            } else {
                                //setMessage(transaction.getMessage());
                                msg = transaction.getMessage();
                            }
                            AlertDialog.Builder builder;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
                            } else {
                                builder = new AlertDialog.Builder(getContext());
                            }
                            builder.setTitle("Order Status")
                                    .setMessage(msg)
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            GetUserOrders();
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<Transaction> call, Throwable throwable) {
                    /*setStatus("transaction_failed");
                    setMessage("Unable to create a transaction. Response Code: " +
                            error.getResponse().getStatus() + " Response body: " +
                            error.getResponse().getBody());*/
                    Toast.makeText(getContext(), "Transaction failed. Unable to create a transaction.", Toast.LENGTH_SHORT);
                    performReset();
                }
            });
        }
    }

    private void performReset() {
        mAuthorization = null;
        if (mBraintreeFragment != null) {
            getFragmentManager().popBackStack();
            mBraintreeFragment = null;
        }
    }

    protected void fetchAuthorization() {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        PaymentClientApi paymentClientApi = retrofit.create(PaymentClientApi.class);
        Call<ClientToken> call = paymentClientApi.getClientToken(mCustomerId, mMerchantAccountId);
        call.enqueue(new Callback<ClientToken>() {
            @Override
            public void onResponse(Call<ClientToken> call, Response<ClientToken> response) {
                if (response.isSuccessful()) {
                    ClientToken clientToken = response.body();
                    if (TextUtils.isEmpty(clientToken.getClientToken())) {
                        Toast.makeText(getContext(), "Client token was empty", Toast.LENGTH_SHORT);
                    } else {
                        mAuthorization = clientToken.getClientToken();
                    }
                }
            }

            @Override
            public void onFailure(Call<ClientToken> call, Throwable throwable) {
                Toast.makeText(getContext(),"Unable to get a client token.", Toast.LENGTH_SHORT);
                Log.e("eApp", throwable.toString());
            }
        });
    }

    public static String getDisplayString(PayPalAccountNonce nonce) {
        return "First name: " + nonce.getFirstName() + "\n" +
                "Last name: " + nonce.getLastName() + "\n" +
                "Email: " + nonce.getEmail() + "\n" +
                "Phone: " + nonce.getPhone() + "\n" +
                "Payer id: " + nonce.getPayerId() + "\n" +
                "Client metadata id: " + nonce.getClientMetadataId() + "\n" +
                "Billing address: " + formatAddress(nonce.getBillingAddress()) + "\n" +
                "Shipping address: " + formatAddress(nonce.getShippingAddress());
    }

    private static String formatAddress(PostalAddress address) {
        return address.getRecipientName() + " " +
                address.getStreetAddress() + " " +
                address.getExtendedAddress() + " " +
                address.getLocality() + " " +
                address.getRegion() + " " +
                address.getPostalCode() + " " +
                address.getCountryCodeAlpha2();
    }
}
