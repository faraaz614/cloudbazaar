package com.app.ecommerce.eapp.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.ecommerce.eapp.Helpers.LocalService;
import com.app.ecommerce.eapp.Helpers.LoginService;
import com.app.ecommerce.eapp.Helpers.Utility;
import com.app.ecommerce.eapp.MainActivity;
import com.app.ecommerce.eapp.Models.AddressResponse;
import com.app.ecommerce.eapp.Models.AddressViewModel;
import com.app.ecommerce.eapp.Models.CategoriesProductsViewModel;
import com.app.ecommerce.eapp.Models.CustomerUpdateResponse;
import com.app.ecommerce.eapp.Models.SingleAddressResponse;
import com.app.ecommerce.eapp.Models.UserModel;
import com.app.ecommerce.eapp.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by RFI on 29-10-2017.
 */

public class AddressFragment extends BaseFragment {
    private static View view;
    List<AddressViewModel> addressViewModels;
    boolean result = false;
    AddressViewModel addressViewModel;
    private static Retrofit retrofit = null;
    public RageComicListFragment.OnRageComicSelected mListener;

    @Override
    protected String getTitle() {
        return "Address";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.address, container, false);
        TextView addaddress = (TextView) view.findViewById(R.id.text_action_bottom2);
        addaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add(new EditAddressFragment());
            }
        });
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        RecyclerView.LayoutManager recylerViewLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(recylerViewLayoutManager);
        recyclerView.setAdapter(new AddressAdapter(view, addressViewModels));
        return view;
    }

    public void SetResult(List<AddressViewModel> addressViewModelList) {
        addressViewModels = addressViewModelList;
    }

    public class AddressAdapter
            extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {

        private List<AddressViewModel> mcartlist;
        private View mView;

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView addressid, flname, email, mobile, cscity;
            public final LinearLayout mLayoutRemove, mLayoutEdit;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                addressid = (TextView) view.findViewById(R.id.addressid);
                flname = (TextView) view.findViewById(R.id.flname);
                email = (TextView) view.findViewById(R.id.email);
                mobile = (TextView) view.findViewById(R.id.mobile);
                cscity = (TextView) view.findViewById(R.id.cscity);
                mLayoutRemove = (LinearLayout) view.findViewById(R.id.layout_action1);
                mLayoutEdit = (LinearLayout) view.findViewById(R.id.layout_action2);
            }
        }

        public AddressAdapter(View view, List<AddressViewModel> cartlist) {
            mcartlist = cartlist;
            mView = view;
        }

        @Override
        public AddressAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.address_list, parent, false);
            return new AddressAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final AddressAdapter.ViewHolder holder, final int position) {
            final AddressViewModel model = mcartlist.get(position);
            if (model != null) {
                holder.addressid.setText(Integer.toString(model.Id()));
                holder.flname.setText(model.FirstName() + " " + model.LastName());
                holder.email.setText(model.Email());
                holder.mobile.setText(model.PhoneNumber());
                holder.cscity.setText(model.City() + " " + model.State() + " " + model.Country());

                holder.mLayoutRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DeleteAddressbyID(model.Id());
                        if (result == true){
                            mcartlist.remove(position);
                            notifyDataSetChanged();
                        }
                        result = false;
                    }
                });

                holder.mLayoutEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        GetUserAddressbyID(model.Id());
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return mcartlist.size();
        }
    }

    public void GetUserAddressbyID(int AddressID) {
        addressViewModel = new AddressViewModel();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        LoginService loginService = retrofit.create(LoginService.class);
        Call<SingleAddressResponse> call = loginService.GetCustomerAddressByID(AddressID);
        call.enqueue(new Callback<SingleAddressResponse>() {
            @Override
            public void onResponse(Call<SingleAddressResponse> call, Response<SingleAddressResponse> response) {
                if (response.body() != null && response.body().Data() != null) {
                    addressViewModel = response.body().Data();
                }
                final EditAddressFragment addressFragment = new EditAddressFragment();
                addressFragment.SetResult(addressViewModel);
                add(addressFragment);
            }

            @Override
            public void onFailure(Call<SingleAddressResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }

    public void DeleteAddressbyID(int AddressID) {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        LoginService loginService = retrofit.create(LoginService.class);
        Call<CustomerUpdateResponse> call = loginService.DeleteCustomerAddress(AddressID);
        call.enqueue(new Callback<CustomerUpdateResponse>() {
            @Override
            public void onResponse(Call<CustomerUpdateResponse> call, Response<CustomerUpdateResponse> response) {
                if (response.errorBody() == null) {
                    result = true;
                }
            }

            @Override
            public void onFailure(Call<CustomerUpdateResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }
}
