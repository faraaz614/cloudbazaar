package com.app.ecommerce.eapp.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RFI on 10-12-2017.
 */

public class BillingAddressModel {
    @SerializedName("FirstName")
    public String FirstName;
    public String FirstName() {
        return FirstName;
    }

    @SerializedName("LastName")
    public String LastName;
    public String LastName() { return LastName; }

    @SerializedName("Email")
    public String Email;
    public String Email() { return Email; }

    @SerializedName("Address1")
    public String Address1;
    public String Address1() { return Address1; }

    @SerializedName("Address2")
    public String Address2;
    public String Address2() { return Address2; }

    @SerializedName("PhoneNumber")
    public String PhoneNumber;
    public String PhoneNumber() { return PhoneNumber; }
}
