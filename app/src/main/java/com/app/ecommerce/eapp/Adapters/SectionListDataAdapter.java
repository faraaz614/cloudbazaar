package com.app.ecommerce.eapp.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ecommerce.eapp.Fragments.RageComicListFragment;
import com.app.ecommerce.eapp.Helpers.Utility;
import com.app.ecommerce.eapp.Models.CategoriesProductsViewModel;
import com.app.ecommerce.eapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SectionListDataAdapter extends RecyclerView.Adapter<SectionListDataAdapter.SingleItemRowHolder> {

    private List<CategoriesProductsViewModel> itemsList;
    private Context mContext;
    private RageComicListFragment.OnRageComicSelected mListener;

    public SectionListDataAdapter(Context context, List<CategoriesProductsViewModel> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
        if (context instanceof RageComicListFragment.OnRageComicSelected) {
            mListener = (RageComicListFragment.OnRageComicSelected) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement OnRageComicSelected.");
        }
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_single_card, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle;
        protected TextView productid;
        protected TextView price;
        protected ImageView itemImage;

        public SingleItemRowHolder(View view) {
            super(view);
            this.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            this.itemImage = (ImageView) view.findViewById(R.id.itemImage);
            this.productid = (TextView) view.findViewById(R.id.productid);
            this.price = (TextView) view.findViewById(R.id.price);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onRageComicSelected(Integer.parseInt(productid.getText().toString()));
                }
            });
        }
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {
        CategoriesProductsViewModel singleItem = itemsList.get(i);
        if (singleItem != null) {
            holder.productid.setText(Integer.toString(singleItem.ProductId()));
            holder.tvTitle.setText(singleItem.ProductName());
            if (singleItem.Price() == null || singleItem.Price().equals("")) {
                holder.price.setText("");
            } else {
                holder.price.setText(Utility.dubaicurrency + singleItem.Price());
            }

            String urll = "";
            if (singleItem != null && singleItem.PictureModel() != null
                    && singleItem.PictureModel().DefaultPictureModel() != null
                    && !singleItem.PictureModel().DefaultPictureModel().FullSizeImageUrl().isEmpty()){
                urll = "http://www.solvemax.in/" + singleItem.PictureModel().DefaultPictureModel().FullSizeImageUrl();
                Picasso.with(mContext).load(urll)
                        //.placeholder(R.drawable.android).error(R.drawable.android)
                        .into(holder.itemImage);
            }
        }
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }
}