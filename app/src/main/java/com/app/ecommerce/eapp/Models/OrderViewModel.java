package com.app.ecommerce.eapp.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RFI on 03-12-2017.
 */

public class OrderViewModel {
    @SerializedName("OrderID")
    public int OrderID;
    public int OrderID() {
        return OrderID;
    }

    @SerializedName("OrderStatus")
    public String OrderStatus;
    public String OrderStatus() { return OrderStatus; }

    @SerializedName("OrderDate")
    public String OrderDate;
    public String OrderDate() { return OrderDate; }

    @SerializedName("OrderPrice")
    public String OrderPrice;
    public String OrderPrice() { return OrderPrice; }

    @SerializedName("Address")
    public String Address;
    public String Address() { return Address; }

    @SerializedName("City")
    public String City;
    public String City() { return City; }

    @SerializedName("ZipCode")
    public String ZipCode;
    public String ZipCode() { return ZipCode; }

    @SerializedName("ShipmentMethod")
    public String ShipmentMethod;
    public String ShipmentMethod() { return ShipmentMethod; }

    @SerializedName("ShipmentStatus")
    public String ShipmentStatus;
    public String ShipmentStatus() { return ShipmentStatus; }

    @SerializedName("PaymentMethod")
    public String PaymentMethod;
    public String PaymentMethod() { return PaymentMethod; }

    @SerializedName("PaymentStatus")
    public String PaymentStatus;
    public String PaymentStatus() { return PaymentStatus; }

    @SerializedName("ProductsList")
    public List<ShoppingCartItemViewModel> ProductsList;
    public List<ShoppingCartItemViewModel> ProductsList() {
        return ProductsList;
    }
    public void Set_ProductsList(List<ShoppingCartItemViewModel> list) {
        ProductsList = list;
    }

    @SerializedName("BillingAddress")
    public BillingAddressModel BillingAddress;
    public BillingAddressModel BillingAddress() {
        return BillingAddress;
    }
    public void Set_BillingAddress(BillingAddressModel list) {
        BillingAddress = list;
    }
}
