package com.app.ecommerce.eapp;

import android.content.SearchRecentSuggestionsProvider;

/**
 * Created by RFI on 26-04-2018.
 */

public class CBSuggestionProvider extends SearchRecentSuggestionsProvider {
    public final static String AUTHORITY = "com.example.CBSuggestionProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public CBSuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}
