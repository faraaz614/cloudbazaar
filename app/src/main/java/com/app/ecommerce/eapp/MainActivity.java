package com.app.ecommerce.eapp;

import android.app.Activity;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.ecommerce.eapp.Adapters.MenuListAdapter;
import com.app.ecommerce.eapp.Adapters.RecyclerViewDataAdapter;
import com.app.ecommerce.eapp.Fragments.AccountFragment;
import com.app.ecommerce.eapp.Fragments.AddressFragment;
import com.app.ecommerce.eapp.Fragments.CartFragment;
import com.app.ecommerce.eapp.Fragments.ContactFragment;
import com.app.ecommerce.eapp.Fragments.HomeFragment;
import com.app.ecommerce.eapp.Fragments.LoginFragment;
import com.app.ecommerce.eapp.Fragments.OrdersFragment;
import com.app.ecommerce.eapp.Fragments.RageComicDetailsFragment;
import com.app.ecommerce.eapp.Fragments.RageComicListFragment;
import com.app.ecommerce.eapp.Fragments.WishListFragment;
import com.app.ecommerce.eapp.Helpers.CategoryService;
import com.app.ecommerce.eapp.Helpers.LocalService;
import com.app.ecommerce.eapp.Helpers.LoginService;
import com.app.ecommerce.eapp.Helpers.ShoppingCartService;
import com.app.ecommerce.eapp.Helpers.Utility;
import com.app.ecommerce.eapp.Models.AddressResponse;
import com.app.ecommerce.eapp.Models.AddressViewModel;
import com.app.ecommerce.eapp.Models.CategoriesProductsViewModel;
import com.app.ecommerce.eapp.Models.CategoryProductResponse;
import com.app.ecommerce.eapp.Models.CategoryandProductViewmodel;
import com.app.ecommerce.eapp.Models.ChildCategory;
import com.app.ecommerce.eapp.Models.OrderViewModel;
import com.app.ecommerce.eapp.Models.OrdersResponse;
import com.app.ecommerce.eapp.Models.ProductDetailResponse;
import com.app.ecommerce.eapp.Models.ProductViewModel;
import com.app.ecommerce.eapp.Models.ShoppingCartItemViewModel;
import com.app.ecommerce.eapp.Models.ShoppingCartResponse;
import com.app.ecommerce.eapp.Models.TEBApiResponse;
import com.app.ecommerce.eapp.Models.UserModel;
import com.app.ecommerce.eapp.Notification.NotificationCountSetClass;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        RecyclerViewDataAdapter.onbtnMoreSelected,
        RageComicListFragment.OnRageComicSelected {

    private ActionBarDrawerToggle toggle;
    private DrawerLayout drawer;
    public static int notificationCountCart = 0;
    private static Retrofit retrofit = null;
    boolean IsLoggedIn = false;
    List<CategoryandProductViewmodel> categoryandProductViewmodelList;
    ExpandableListView expListView;
    public List<ProductViewModel> productViewModels;
    public CategoriesProductsViewModel productViewModel;
    public List<CategoriesProductsViewModel> shoppingCartItemViewModels;
    public List<ShoppingCartItemViewModel> listCartModel;
    List<AddressViewModel> addressViewModels;
    public List<OrderViewModel> orderViewModels;
    public AlertDialog levelDialog;
    final CharSequence[] items = {" Name: A to Z ", " Name : Z to A", " Price : Low to High", " Price : High to Low"};
    public int categoryid = 1;
    private ProgressBar pgsBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pgsBar = (ProgressBar) findViewById(R.id.pBar);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, 0, 0) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                setDrawerIndicatorEnabled(true);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        UserModel user = new LocalService(this).GetUser();
        if (user != null) {
            IsLoggedIn = true;
        }

        EditText searchtxt = (EditText) findViewById(R.id.searchtxt);
        searchtxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    //performSearch();
                    Intent i = new Intent(getApplicationContext(), SearchActivity.class);
                    i.putExtra(Utility.searchtxt, v.getText().toString());
                    startActivity(i);
                    return true;
                }
                return false;
            }
        });

        categoryandProductViewmodelList = new ArrayList<>();
        GetDataFromApi();
        expListView = (ExpandableListView) findViewById(R.id.category_list);
        final ExpandableListAdapter expListAdapter = new MenuListAdapter(MainActivity.this, categoryandProductViewmodelList);
        expListView.setAdapter(expListAdapter);

        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                List<ChildCategory> children = categoryandProductViewmodelList.get(i).ChildCategory();
                if (children == null || children.size() == 0) {
                    TextView parentcategoryid = (TextView) view.findViewById(R.id.parentcategoryid);
                    categoryid = Integer.parseInt(parentcategoryid.getText().toString());
                    GetCategoryProduct(categoryid);
                    CloseDrawer();
                    return true;
                } else {
                    return false;
                }
            }
        });

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                TextView childcategoryid = (TextView) v.findViewById(R.id.childcategoryid);
                categoryid = Integer.parseInt(childcategoryid.getText().toString());
                GetCategoryProduct(categoryid);
                CloseDrawer();
                return true;
            }
        });
    }

    @Override
    protected ActionBarDrawerToggle getDrawerToggle() {
        return toggle;
    }

    @Override
    protected DrawerLayout getDrawer() {
        return drawer;
    }

    public void onbtnMoreSelected(Context context, String categoryId) {
        categoryid = Integer.parseInt(categoryId);
        GetCategoryProduct(categoryid);
    }

    public void onRageComicSelected(int productId) {
        GetProductDetailsByID(productId);
    }

    public void CloseDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
/*
        MenuItem searchItem = menu.findItem(R.id.search);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
*/

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (IsLoggedIn) {
            menu.findItem(R.id.action_signin).setVisible(false);
            menu.findItem(R.id.action_signout).setVisible(true);
        }
        // Get the notifications MenuItem and
        // its LayerDrawable (layer-list)
        //notificationCountCart = new LocalService(this).CartCount();
        MenuItem item = menu.findItem(R.id.cart);
        new NotificationCountSetClass().setAddToCart(MainActivity.this, item, notificationCountCart);
        // force the ActionBar to relayout its MenuItems.
        // onCreateOptionsMenu(Menu) will be called again.
        invalidateOptionsMenu();
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search) {
            //onSearchRequested();
        } else if (id == R.id.action_contact) {
            add(new ContactFragment());
        } else if (id == R.id.filter) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Sort By");
            builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    switch (item) {
                        case 0:
                            GetCategoryProduct(categoryid, "A to Z");
                            break;
                        case 1:
                            GetCategoryProduct(categoryid, "Z to A");
                            break;
                        case 2:
                            GetCategoryProduct(categoryid, "Low to High");
                            break;
                        case 3:
                            GetCategoryProduct(categoryid, "High to Low");
                            break;

                    }
                    levelDialog.dismiss();
                }
            });
            levelDialog = builder.create();
            levelDialog.show();
        } else if (!IsLoggedIn || id == R.id.action_signin) {
            add(new LoginFragment());
        } else if (id == R.id.action_signout) {
            new LocalService(this).SignOut();
            IsLoggedIn = false;
        } else if (id == R.id.cart) {
            GetUserCart();
        } else if (id == R.id.action_myaccount) {
            add(new AccountFragment());
        } else if (id == R.id.action_address) {
            GetUserAddress();
        } else if (id == R.id.action_wishlist) {
            GetUserWishList();
        } else if (id == R.id.action_orders) {
            GetUserOrders();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void GetDataFromApi() {

        pgsBar.setVisibility(View.VISIBLE);

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        CategoryService categoryService = retrofit.create(CategoryService.class);
        Call<TEBApiResponse> call = categoryService.GetAllCategoryAndProducts();
        call.enqueue(new Callback<TEBApiResponse>() {
            @Override
            public void onResponse(Call<TEBApiResponse> call, Response<TEBApiResponse> response) {
                List<CategoryandProductViewmodel> homeList = new ArrayList<CategoryandProductViewmodel>();
                HomeFragment homeImportFragment = new HomeFragment();
                if (response.body() != null && response.body().Data() != null) {
                    categoryandProductViewmodelList = response.body().Data();
                    final ExpandableListAdapter expListAdapter = new MenuListAdapter(MainActivity.this, categoryandProductViewmodelList);
                    expListView.setAdapter(expListAdapter);
                    for (CategoryandProductViewmodel model : categoryandProductViewmodelList) {
                        if (model.ProductList().size() > 0) {
                            homeList.add(model);
                        }
                    }
                }
                homeImportFragment.SetResult(homeList);
                add(homeImportFragment);
                pgsBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<TEBApiResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }

    public void GetCategoryProduct(int CategoryID) {

        pgsBar.setVisibility(View.VISIBLE);
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        CategoryService categoryService = retrofit.create(CategoryService.class);
        Call<CategoryProductResponse> call = categoryService.GetProductByCategoryID(CategoryID, 1);
        call.enqueue(new Callback<CategoryProductResponse>() {
            @Override
            public void onResponse(Call<CategoryProductResponse> call, Response<CategoryProductResponse> response) {
                if (response.body() != null && response.body().Data() != null) {
                    productViewModels = response.body().Data();
                    RageComicListFragment rageComicListFragment = new RageComicListFragment();
                    rageComicListFragment.SetResult(productViewModels);
                    add(rageComicListFragment);
                    pgsBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<CategoryProductResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }

    public void GetProductDetailsByID(int ID) {

        pgsBar.setVisibility(View.VISIBLE);

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        CategoryService categoryService = retrofit.create(CategoryService.class);
        Call<ProductDetailResponse> call = categoryService.GetProductDetailsByID(ID);
        call.enqueue(new Callback<ProductDetailResponse>() {
            @Override
            public void onResponse(Call<ProductDetailResponse> call, Response<ProductDetailResponse> response) {
                if (response.body() != null && response.body().Data() != null) {
                    productViewModel = response.body().Data();
                    final RageComicDetailsFragment detailsFragment = new RageComicDetailsFragment();
                    detailsFragment.SetResult(productViewModel);
                    add(detailsFragment);
                    pgsBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ProductDetailResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }

    public void GetUserCart() {

        pgsBar.setVisibility(View.VISIBLE);
        listCartModel = new ArrayList<>();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        UserModel userModel = new LocalService(this).GetUser();
        ShoppingCartService shoppingCartService = retrofit.create(ShoppingCartService.class);
        Call<ShoppingCartResponse> call = shoppingCartService.GetShoppingCart(userModel.getUserID(), Utility.shoppingCartTypeIdCart);
        call.enqueue(new Callback<ShoppingCartResponse>() {
            @Override
            public void onResponse(Call<ShoppingCartResponse> call, Response<ShoppingCartResponse> response) {
                if (response.body() != null && response.body().Data() != null) {
                    listCartModel = response.body().Data();
                }
                FragmentManager fm = getSupportFragmentManager();
                final CartFragment cartFragment = new CartFragment();
                cartFragment.SetResult(listCartModel);
                add(cartFragment);
                pgsBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ShoppingCartResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }

    public void GetUserAddress() {

        pgsBar.setVisibility(View.VISIBLE);
        addressViewModels = new ArrayList<>();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        UserModel userModel = new LocalService(this).GetUser();
        LoginService loginService = retrofit.create(LoginService.class);
        Call<AddressResponse> call = loginService.GetCustomerAddress(userModel.getUserID());
        call.enqueue(new Callback<AddressResponse>() {
            @Override
            public void onResponse(Call<AddressResponse> call, Response<AddressResponse> response) {
                if (response.body() != null && response.body().Data() != null) {
                    addressViewModels = response.body().Data();
                }
                final AddressFragment addressFragment = new AddressFragment();
                addressFragment.SetResult(addressViewModels);
                add(addressFragment);
                pgsBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<AddressResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }

    public void GetUserWishList() {

        pgsBar.setVisibility(View.VISIBLE);
        listCartModel = new ArrayList<>();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        UserModel userModel = new LocalService(this).GetUser();
        ShoppingCartService shoppingCartService = retrofit.create(ShoppingCartService.class);
        Call<ShoppingCartResponse> call = shoppingCartService.GetShoppingCart(userModel.getUserID(), Utility.shoppingCartTypeIdWish);
        call.enqueue(new Callback<ShoppingCartResponse>() {
            @Override
            public void onResponse(Call<ShoppingCartResponse> call, Response<ShoppingCartResponse> response) {
                if (response.body() != null && response.body().Data() != null) {
                    listCartModel = response.body().Data();
                }
                FragmentManager fm = getSupportFragmentManager();
                final WishListFragment wishListFragment = new WishListFragment();
                wishListFragment.SetResult(listCartModel);
                add(wishListFragment);
                pgsBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ShoppingCartResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }

    public void GetUserOrders() {

        pgsBar.setVisibility(View.VISIBLE);
        orderViewModels = new ArrayList<>();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        UserModel userModel = new LocalService(this).GetUser();
        ShoppingCartService shoppingCartService = retrofit.create(ShoppingCartService.class);
        Call<OrdersResponse> call = shoppingCartService.GetCustomerOrders(userModel.getUserID());
        call.enqueue(new Callback<OrdersResponse>() {
            @Override
            public void onResponse(Call<OrdersResponse> call, Response<OrdersResponse> response) {
                if (response.body() != null && response.body().Data() != null) {
                    orderViewModels = response.body().Data();
                    OrdersFragment ordersFragment = new OrdersFragment();
                    ordersFragment.SetResult(orderViewModels);
                    add(ordersFragment);
                    pgsBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<OrdersResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }

    public void GetCategoryProduct(int CategoryID, String sort) {

        pgsBar.setVisibility(View.VISIBLE);
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        CategoryService categoryService = retrofit.create(CategoryService.class);
        Call<CategoryProductResponse> call = categoryService.GetProductByCategoryID(CategoryID, 1, null, sort);
        call.enqueue(new Callback<CategoryProductResponse>() {
            @Override
            public void onResponse(Call<CategoryProductResponse> call, Response<CategoryProductResponse> response) {
                if (response.body() != null && response.body().Data() != null) {
                    productViewModels = response.body().Data();
                    RageComicListFragment rageComicListFragment = new RageComicListFragment();
                    rageComicListFragment.SetResult(productViewModels);
                    add(rageComicListFragment);
                    pgsBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<CategoryProductResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }
}
