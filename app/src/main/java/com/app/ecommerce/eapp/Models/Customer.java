package com.app.ecommerce.eapp.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RFI on 24-10-2017.
 */

public class Customer {
    @SerializedName("Id")
    public int Id;
    public int Id() {
        return Id;
    }

    @SerializedName("Username")
    public String Username;
    public String Username() { return Username; }
}
