package com.app.ecommerce.eapp.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RFI on 24-10-2017.
 */

public class Product {
    @SerializedName("Id")
    public int Id;
    public int Id() {
        return Id;
    }

    @SerializedName("Name")
    public String Name;
    public String Name() { return Name; }

    @SerializedName("FullDescription")
    public String FullDescription;
    public String FullDescription() {
        return FullDescription;
    }

    @SerializedName("StockQuantity")
    public int StockQuantity;
    public int StockQuantity() {
        return StockQuantity;
    }

    @SerializedName("IsShipEnabled")
    public boolean IsShipEnabled;
    public boolean IsShipEnabled() {
        return IsShipEnabled;
    }

    @SerializedName("IsFreeShipping")
    public boolean IsFreeShipping;
    public boolean IsFreeShipping() {
        return IsFreeShipping;
    }

    @SerializedName("AdditionalShippingCharge")
    public int AdditionalShippingCharge;
    public int AdditionalShippingCharge() {
        return AdditionalShippingCharge;
    }

    @SerializedName("CallForPrice")
    public boolean CallForPrice;
    public boolean CallForPrice() {
        return CallForPrice;
    }

    @SerializedName("Price")
    public int Price;
    public int Price() {
        return Price;
    }

    @SerializedName("OldPrice")
    public int OldPrice;
    public int OldPrice() {
        return OldPrice;
    }

    @SerializedName("ProductCost")
    public int ProductCost;
    public int ProductCost() {
        return ProductCost;
    }

    @SerializedName("CustomerEntersPrice")
    public boolean CustomerEntersPrice;
    public boolean CustomerEntersPrice() {
        return CustomerEntersPrice;
    }

    @SerializedName("MinimumCustomerEnteredPrice")
    public int MinimumCustomerEnteredPrice;
    public int MinimumCustomerEnteredPrice() {
        return MinimumCustomerEnteredPrice;
    }

    @SerializedName("MaximumCustomerEnteredPrice")
    public int MaximumCustomerEnteredPrice;
    public int MaximumCustomerEnteredPrice() {
        return MaximumCustomerEnteredPrice;
    }

    @SerializedName("BasepriceEnabled")
    public boolean BasepriceEnabled;
    public boolean BasepriceEnabled() {
        return BasepriceEnabled;
    }

    @SerializedName("BasepriceAmount")
    public int BasepriceAmount;
    public int BasepriceAmount() {
        return BasepriceAmount;
    }

    @SerializedName("BasepriceUnitId")
    public int BasepriceUnitId;
    public int BasepriceUnitId() {
        return BasepriceUnitId;
    }

    @SerializedName("BasepriceBaseAmount")
    public int BasepriceBaseAmount;
    public int BasepriceBaseAmount() {
        return BasepriceBaseAmount;
    }
}
