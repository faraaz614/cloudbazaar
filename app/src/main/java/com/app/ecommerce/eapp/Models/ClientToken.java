package com.app.ecommerce.eapp.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RFI on 22-07-2018.
 */

public class ClientToken {
    @SerializedName("client_token")
    private String mClientToken;

    public String getClientToken() {
        return mClientToken;
    }
}
