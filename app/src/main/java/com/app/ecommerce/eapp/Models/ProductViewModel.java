package com.app.ecommerce.eapp.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RFI on 25-10-2017.
 */

public class ProductViewModel {
    @SerializedName("ProductId")
    public int ProductId;

    public int ProductId() {
        return ProductId;
    }

    @SerializedName("ProductName")
    public String ProductName;

    public String ProductName() {
        return ProductName;
    }

    @SerializedName("Price")
    public double Price;

    public double Price() {
        return Price;
    }

    @SerializedName("PicBinary")
    public String PicBinary;

    public String PicBinary() {
        return PicBinary;
    }

    @SerializedName("RelateProducts")
    public List<ProductViewModel> RelateProducts;

    public List<ProductViewModel> RelateProducts() {
        return RelateProducts;
    }

    @SerializedName("PictureModel")
    public ProductDetailsModel PictureModel;
    public ProductDetailsModel PictureModel() { return PictureModel; }
}
