package com.app.ecommerce.eapp.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.ecommerce.eapp.Helpers.ShoppingCartService;
import com.app.ecommerce.eapp.Helpers.Utility;
import com.app.ecommerce.eapp.Models.CategoriesProductsViewModel;
import com.app.ecommerce.eapp.Models.OrderViewModel;
import com.app.ecommerce.eapp.Models.OrdersDetailResponse;
import com.app.ecommerce.eapp.Models.OrdersResponse;
import com.app.ecommerce.eapp.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by RFI on 30-10-2017.
 */

public class OrdersFragment extends BaseFragment {
    private static View view;
    List<OrderViewModel> orderViewModels;
    OrderViewModel orderViewModel;
    private static Retrofit retrofit = null;

    public RageComicListFragment.OnRageComicSelected mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof RageComicListFragment.OnRageComicSelected) {
            mListener = (RageComicListFragment.OnRageComicSelected) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement OnRageComicSelected.");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.orders, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        RecyclerView.LayoutManager recylerViewLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(recylerViewLayoutManager);
        recyclerView.setAdapter(new OrdersAdapter(view, orderViewModels));
        return view;
    }

    @Override
    protected String getTitle() {
        return "Orders";
    }

    public void SetResult(List<OrderViewModel> list) {
        orderViewModels = list;
    }

    public class OrdersAdapter
            extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {

        private List<OrderViewModel> mcartlist;
        private View mView;

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView orderno, orderstatus,orderdate,ordertotal;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                orderno = (TextView) view.findViewById(R.id.orderno);
                orderstatus = (TextView) view.findViewById(R.id.orderstatus);
                orderdate = (TextView) view.findViewById(R.id.orderdate);
                ordertotal = (TextView) view.findViewById(R.id.ordertotal);
            }
        }

        public OrdersAdapter(View view, List<OrderViewModel> list) {
            mcartlist = list;
            mView = view;
        }

        @Override
        public OrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.orders_list, parent, false);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    TextView orderid = (TextView)view.findViewById(R.id.orderno);
                    GetOrderDetails(Integer.parseInt(orderid.getText().toString()));
                }
            });
            return new OrdersAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final OrdersAdapter.ViewHolder holder, final int position) {
            final OrderViewModel model = orderViewModels.get(position);
            if (model != null) {
                holder.orderno.setText(Integer.toString(model.OrderID()));
                holder.orderstatus.setText(model.OrderStatus());
                holder.orderdate.setText(model.OrderDate());
                holder.ordertotal.setText(model.OrderPrice());
            }
        }

        @Override
        public int getItemCount() {
            return mcartlist.size();
        }
    }

    public void GetOrderDetails(Integer orderno) {
        orderViewModel = new OrderViewModel();
        final CategoriesProductsViewModel categoriesProductsViewModel = new CategoriesProductsViewModel();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        ShoppingCartService shoppingCartService = retrofit.create(ShoppingCartService.class);
        Call<OrdersDetailResponse> call = shoppingCartService.GetSingleOrder(orderno);
        call.enqueue(new Callback<OrdersDetailResponse>() {
            @Override
            public void onResponse(Call<OrdersDetailResponse> call, Response<OrdersDetailResponse> response) {
                if (response.body() != null && response.body().Data() != null) {
                    orderViewModel = response.body().Data();
                    OrderDetailsFragment orderDetailsFragment = new OrderDetailsFragment();
                    orderDetailsFragment.SetResult(orderViewModel);
                    add(orderDetailsFragment);
                }
            }

            @Override
            public void onFailure(Call<OrdersDetailResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }
}