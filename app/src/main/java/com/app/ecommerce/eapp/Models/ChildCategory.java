package com.app.ecommerce.eapp.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RFI on 24-10-2017.
 */

public class ChildCategory {
    @SerializedName("CategoryId")
    private int CategoryId;
    @SerializedName("CategoryName")
    private String CategoryName;

    public int CategoryId() {
        return CategoryId;
    }

    public String CategoryName() {
        return CategoryName;
    }
}
