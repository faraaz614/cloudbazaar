package com.app.ecommerce.eapp.Fragments;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.app.ecommerce.eapp.Adapters.RecyclerViewDataAdapter;
import com.app.ecommerce.eapp.Helpers.CustomLinearLayoutManager;
import com.app.ecommerce.eapp.MainActivity;
import com.app.ecommerce.eapp.Models.CategoryandProductViewmodel;
import com.app.ecommerce.eapp.R;

import java.util.ArrayList;
import java.util.List;

import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.events.OnBannerClickListener;
import ss.com.bannerslider.views.BannerSlider;

public class HomeFragment extends BaseFragment {

    String bannerlist[] = { "http://www.solvemax.in/content/images/thumbs/0000083.jpeg",
                            "http://www.solvemax.in/content/images/thumbs/0000090.jpeg",
                            "http://www.solvemax.in/content/images/thumbs/0000092.jpeg" };

    List<CategoryandProductViewmodel> _categoryProductList;
    private RecyclerViewDataAdapter.onbtnMoreSelected mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof RecyclerViewDataAdapter.onbtnMoreSelected ) {
            mListener = (RecyclerViewDataAdapter.onbtnMoreSelected) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement OnRageComicSelected.");
        }
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View v = inflater.inflate(R.layout.home,container,false);

        BannerSlider bannerSlider = (BannerSlider) v.findViewById(R.id.homebanner);
        List<Banner> banners=new ArrayList<>();
        banners.add(new RemoteBanner(bannerlist[0]));
        banners.add(new RemoteBanner(bannerlist[1]));
        banners.add(new RemoteBanner(bannerlist[2]));
        bannerSlider.setBanners(banners);
        bannerSlider.setOnBannerClickListener(new OnBannerClickListener() {
            @Override
            public void onClick(int position) {
                int categoryid = 13;
                if (position == 1){
                    categoryid=14;
                } else if (position == 2) {
                    categoryid = 15;
                }
                mListener.onbtnMoreSelected(getContext(), Integer.toString(categoryid));
            }
        });

        RecyclerView my_recycler_view = (RecyclerView) v.findViewById(R.id.horizontal_recycler_view);
        my_recycler_view.setHasFixedSize(true);
        RecyclerViewDataAdapter adapter = new RecyclerViewDataAdapter(getContext(), _categoryProductList);
        final CustomLinearLayoutManager layoutManager = new CustomLinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        my_recycler_view.setLayoutManager(layoutManager);
        my_recycler_view.setAdapter(adapter);
        my_recycler_view.setNestedScrollingEnabled(false);
        return v;
    }

    @Override
    protected String getTitle() {
        return "Cloudbazaar";
    }

    public void SetResult(List<CategoryandProductViewmodel> categoryProductList){
        _categoryProductList = categoryProductList;
    }
}
