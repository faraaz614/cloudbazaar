package com.app.ecommerce.eapp.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RFI on 24-10-2017.
 */

public class CategoriesProductsViewModel {
    @SerializedName("ProductId")
    private int ProductId;
    public int ProductId() { return ProductId; }
    public void SetProductId(int productid) { ProductId = productid; }

    @SerializedName("ProductName")
    private String ProductName;
    public String ProductName() {
        return ProductName;
    }
    public void SetProductName(String productName) { ProductName = productName; }

    @SerializedName("Price")
    private String Price;
    public String Price() {
        return Price;
    }
    public void SetPrice(String price) { Price = price; }

    @SerializedName("FullDescription")
    private String FullDescription;
    public String FullDescription() {
        return FullDescription;
    }

    @SerializedName("ShortDescription")
    private String ShortDescription;
    public String ShortDescription() {
        return ShortDescription;
    }
    public void SetShortDescription(String shortDescription) { ShortDescription = shortDescription; }

    @SerializedName("PicBinary")
    public String PicBinary;
    public String PicBinary() {
        return PicBinary;
    }
    public void SetPicBinary(String picBinary) {
        PicBinary = picBinary;
    }

    @SerializedName("ImageURL")
    private String ImageURL;
    public String ImageURL() {
        return ImageURL;
    }
    public void SetImageURL(String imageURL) { ImageURL = imageURL; }

    @SerializedName("Quantity")
    private String Quantity;
    public String Quantity() { return Quantity; }
    public void SetQuantity(String quantity) { Quantity = quantity; }

    @SerializedName("Pictures")
    public List<String> Pictures;
    public List<String> Pictures() {
        return Pictures;
    }

    @SerializedName("RelateProducts")
    public List<CategoriesProductsViewModel> RelateProducts;
    public List<CategoriesProductsViewModel> RelateProducts() {
        return RelateProducts;
    }

    @SerializedName("PictureModel")
    public ProductDetailsModel PictureModel;
    public ProductDetailsModel PictureModel() { return PictureModel; }
    public void SetProductDetailsModel(ProductDetailsModel pictureModel) { PictureModel = pictureModel; }
}
