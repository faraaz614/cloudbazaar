package com.app.ecommerce.eapp.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.app.ecommerce.eapp.Helpers.LoginService;
import com.app.ecommerce.eapp.Helpers.Utility;
import com.app.ecommerce.eapp.Models.CustomerResponse;
import com.app.ecommerce.eapp.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by RFI on 29-10-2017.
 */

public class AccountFragment extends BaseFragment {
    private static Retrofit retrofit = null;
    private static View view;
    private static EditText currentpassword, newpassword, confirmnewpassword;
    private static TextView lblmsg;
    private static Button changepwd;

    @Override
    protected String getTitle() {
        return "Account";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.account, container, false);
        currentpassword = (EditText) view.findViewById(R.id.currentpassword);
        newpassword = (EditText) view.findViewById(R.id.newpassword);
        confirmnewpassword = (EditText) view.findViewById(R.id.confirmnewpassword);
        changepwd = (Button) view.findViewById(R.id.changepwd);
        lblmsg = (TextView) view.findViewById(R.id.lblmsg);
        changepwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPwdValidation();
            }
        });
        return view;
    }

    private void checkPwdValidation() {
        String getcurrentpassword = currentpassword.getText().toString();
        String getPassword = newpassword.getText().toString();
        String getConfirmPassword = confirmnewpassword.getText().toString();

        if (getcurrentpassword.equals("") || getcurrentpassword.length() == 0
                || getPassword.equals("") || getPassword.length() == 0
                || getConfirmPassword.equals("")
                || getConfirmPassword.length() == 0){
            lblmsg.setText("All fields are required.");
        } else if (!getConfirmPassword.equals(getPassword)) {
            lblmsg.setText("Both new passwords doesn't match.");
        } else {
            ApiChangePassword(getcurrentpassword, getPassword);
        }
    }

    public void ApiChangePassword(String getFullName, String getEmailId) {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        LoginService loginService = retrofit.create(LoginService.class);
        Call<CustomerResponse> call = loginService.PostCustomer(getFullName, getEmailId, "");
        call.enqueue(new Callback<CustomerResponse>() {
            @Override
            public void onResponse(Call<CustomerResponse> call, Response<CustomerResponse> response) {
                CustomerResponse customer = response.body();
                if (customer != null && customer.Data() != null) {
                    lblmsg.setText("Password Updated.");
                } else {
                    lblmsg.setText("Invalid details.");
                }
            }

            @Override
            public void onFailure(Call<CustomerResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }
}