package com.app.ecommerce.eapp.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RFI on 24-10-2017.
 */

public class ShoppingCartItemViewModel {
    @SerializedName("Id")
    public int Id;
    public int Id() {
        return Id;
    }
    public void Set_Id(int id) {
        Id = id;
    }

    @SerializedName("StoreId")
    public int StoreId;
    public int StoreId() {
        return StoreId;
    }
    public void Set_StoreId(int storeid) {
        StoreId = storeid;
    }

    @SerializedName("ShoppingCartTypeId")
    public int ShoppingCartTypeId;
    public int ShoppingCartTypeId() {
        return ShoppingCartTypeId;
    }
    public void Set_ShoppingCartTypeId(int shoppingCartTypeId) {
        ShoppingCartTypeId = shoppingCartTypeId;
    }

    @SerializedName("CustomerId")
    public int CustomerId;
    public int CustomerId() {
        return CustomerId;
    }
    public void Set_CustomerId(int customerId) {
        CustomerId = customerId;
    }

    @SerializedName("ProductId")
    public int ProductId;
    public int ProductId() {
        return ProductId;
    }
    public void Set_ProductId(int productId) {
        ProductId = productId;
    }

    @SerializedName("AttributesXml")
    public String AttributesXml;
    public String AttributesXml() { return AttributesXml; }
    public void Set_AttributesXml(String attributesXml) {
        AttributesXml = attributesXml;
    }

    @SerializedName("CustomerEnteredPrice")
    public int CustomerEnteredPrice;
    public int CustomerEnteredPrice() { return CustomerEnteredPrice; }
    public void Set_CustomerEnteredPrice(int customerEnteredPrice) {
        CustomerEnteredPrice = customerEnteredPrice;
    }

    @SerializedName("Quantity")
    public int Quantity;
    public int Quantity() { return Quantity; }
    public void Set_Quantity(int quantity) {
        Quantity = quantity;
    }

    @SerializedName("ProductName")
    public String ProductName;
    public String ProductName() {
        return ProductName;
    }
    public void Set_ProductName(String productName) {
        ProductName = productName;
    }

    @SerializedName("Price")
    public String Price;
    public String Price() {
        return Price;
    }
    public void Set_Price(String price) {
        Price = price;
    }

    @SerializedName("ShortDescription")
    public String ShortDescription;
    public String ShortDescription() {
        return ShortDescription;
    }
    public void Set_ShortDescription(String shortDescription) {
        ShortDescription = shortDescription;
    }

    @SerializedName("Description")
    public String Description;
    public String Description() {
        return Description;
    }
    public void Set_Description(String description) {
        Description = description;
    }

    @SerializedName("PictureBinary")
    public String PictureBinary;
    public String PictureBinary() {
        return PictureBinary;
    }
    public void Set_PictureBinary(String pictureBinary) {
        PictureBinary = pictureBinary;
    }

    @SerializedName("Removefromcart")
    public boolean Removefromcart;
    public boolean Removefromcart() {
        return Removefromcart;
    }
    public void Set_Removefromcart(boolean removefromcart) {
        Removefromcart = removefromcart;
    }

    @SerializedName("ItesList")
    public List<ShoppingCartItemViewModel> ItesList;
    public List<ShoppingCartItemViewModel> ItesList() {
        return ItesList;
    }
    public void Set_ItesList(List<ShoppingCartItemViewModel> itesList) {
        ItesList = itesList;
    }

    @SerializedName("PictureModel")
    public ProductDetailsModel PictureModel;
    public ProductDetailsModel PictureModel() { return PictureModel; }
}
