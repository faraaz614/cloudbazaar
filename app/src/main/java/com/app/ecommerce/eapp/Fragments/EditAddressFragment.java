package com.app.ecommerce.eapp.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.app.ecommerce.eapp.Helpers.LocalService;
import com.app.ecommerce.eapp.Helpers.LoginService;
import com.app.ecommerce.eapp.Helpers.Utility;
import com.app.ecommerce.eapp.Models.AddressViewModel;
import com.app.ecommerce.eapp.Models.CustomerResponse;
import com.app.ecommerce.eapp.Models.CustomerUpdateResponse;
import com.app.ecommerce.eapp.Models.UserModel;
import com.app.ecommerce.eapp.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by RFI on 08-11-2017.
 */

public class EditAddressFragment extends BaseFragment {
    private static View view;
    private static TextView lblmsg, addressid;
    private static EditText firstname, lastname, email, company, city, address1, address2, pin, mobile, fax;
    private static Button save;
    private static Spinner country, state;
    AddressViewModel addressViewModel;
    private static Retrofit retrofit = null;
    private static int _countryid = 0;
    private static int _stateid = 0;

    @Override
    protected String getTitle() {
        return "Edit Address";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.address_details, container, false);
        addressid = (TextView) view.findViewById(R.id.addressid);
        lblmsg = (TextView) view.findViewById(R.id.lblmsg);
        firstname = (EditText) view.findViewById(R.id.firstname);
        lastname = (EditText) view.findViewById(R.id.lastname);
        email = (EditText) view.findViewById(R.id.email);
        company = (EditText) view.findViewById(R.id.company);
        city = (EditText) view.findViewById(R.id.city);
        address1 = (EditText) view.findViewById(R.id.address1);
        address2 = (EditText) view.findViewById(R.id.address2);
        pin = (EditText) view.findViewById(R.id.pin);
        mobile = (EditText) view.findViewById(R.id.mobile);
        fax = (EditText) view.findViewById(R.id.fax);
        country = (Spinner) view.findViewById(R.id.country);
        state = (Spinner) view.findViewById(R.id.state);
        save = (Button) view.findViewById(R.id.save);

        ArrayAdapter<String> countrySpinerAdapter;
        String[] countries = {"Select", "United Arab Emirates"};
        final int[] countriesValues = {0, 81};

        countrySpinerAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, countries);
        country.setAdapter(countrySpinerAdapter);
        country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                _countryid = countriesValues[arg2];
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        ArrayAdapter<String> stateSpinerAdapter;
        String[] states = {"Select", "Abu Dhabi"};
        final int[] statesValues = {0, 76};

        stateSpinerAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, states);
        state.setAdapter(stateSpinerAdapter);
        state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                _stateid = statesValues[arg2];
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkValidation();
            }
        });
        if (addressViewModel != null && addressViewModel.Id() > 0) {
            setvalues();
        }
        return view;
    }

    private void setvalues() {
        addressid.setText(Integer.toString(addressViewModel.Id()));
        firstname.setText(addressViewModel.FirstName());
        lastname.setText(addressViewModel.LastName());
        email.setText(addressViewModel.Email());
        company.setText(addressViewModel.Company());
        city.setText(addressViewModel.City());
        address1.setText(addressViewModel.Address1());
        address2.setText(addressViewModel.Address2());
        pin.setText(addressViewModel.ZipPostalCode());
        mobile.setText(addressViewModel.PhoneNumber());
        fax.setText(addressViewModel.FaxNumber());
        country.setId(addressViewModel.CountryId());
        state.setId(addressViewModel.StateprovinceId());
    }

    private void checkValidation() {
        String getaddressid = addressid.getText().toString();
        String getfirstname = firstname.getText().toString();
        String getlastname = lastname.getText().toString();
        String getemail = email.getText().toString();
        String getcompany = company.getText().toString();
        String getcity = city.getText().toString();
        String getaddress1 = address1.getText().toString();
        String getaddress2 = address2.getText().toString();
        String getpin = pin.getText().toString();
        String getmobile = mobile.getText().toString();
        String getfax = fax.getText().toString();
        String getcountry = Integer.toString(_countryid);
        String getstate = Integer.toString(_stateid);

        Pattern p = Pattern.compile(Utility.regEx);
        Matcher m = p.matcher(getemail);

        if (getfirstname.equals("") || getfirstname.length() == 0
                || getlastname.equals("") || getlastname.length() == 0
                || getemail.equals("") || getemail.length() == 0
                || getcompany.equals("") || getcompany.length() == 0
                || getcity.equals("") || getcity.length() == 0
                || getaddress1.equals("") || getaddress1.length() == 0
                || getaddress2.equals("") || getaddress2.length() == 0
                || getpin.equals("") || getpin.length() == 0
                || getmobile.equals("") || getmobile.length() == 0
                || getfax.equals("") || getfax.length() == 0
                || getcountry.equals("") || getcountry.length() == 0
                || getstate.equals("") || getstate.length() == 0) {
            lblmsg.setText("All fields are required.");
        } else if (!m.find()) {
            lblmsg.setText("Your Email Id is Invalid.");
        } else {
            if (getaddressid.equals("") || getaddressid.length() == 0){
                ApiAddAddress(getfirstname, getlastname, getemail, getcompany, getcity,
                        getaddress1, getaddress2, getpin, getmobile,
                        getfax, getcountry, getstate);
            } else {
                ApiUpdateAddress(getfirstname, getlastname, getemail, getcompany, getcity,
                        getaddress1, getaddress2, getpin, getmobile,
                        getfax, getcountry, getstate, getaddressid);
            }
        }
    }

    public void SetResult(AddressViewModel addressViewModelList) {
        addressViewModel = addressViewModelList;
    }

    public void ApiUpdateAddress(String getfirstname, String getlastname, String getemail, String getcompany, String getcity,
                              String getaddress1, String getaddress2, String getpin, String getmobile,
                              String getfax, String getcountry, String getstate, String getaddressid) {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        LoginService loginService = retrofit.create(LoginService.class);
        Call<CustomerUpdateResponse> call = loginService.UpdateCustomerAddress(getfirstname, getlastname, getemail, getcompany, getcountry,
                getstate, getcity, getaddress1, getaddress2, getpin, getmobile, getfax, Integer.parseInt(getaddressid));
        call.enqueue(new Callback<CustomerUpdateResponse>() {
            @Override
            public void onResponse(Call<CustomerUpdateResponse> call, Response<CustomerUpdateResponse> response) {
                if (response.errorBody() == null) {
                    lblmsg.setText("Address Updated.");
                } else {
                    lblmsg.setText("Invalid details.");
                }
            }

            @Override
            public void onFailure(Call<CustomerUpdateResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }

    public void ApiAddAddress(String getfirstname, String getlastname, String getemail, String getcompany, String getcity,
                              String getaddress1, String getaddress2, String getpin, String getmobile,
                              String getfax, String getcountry, String getstate) {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utility.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        UserModel userModel = new UserModel();
        userModel = new LocalService(getContext()).GetUser();
        LoginService loginService = retrofit.create(LoginService.class);
        Call<CustomerResponse> call = loginService.AddCustomerAddress(getfirstname, getlastname, getemail, getcompany, getcountry,
                getstate, getcity, getaddress1, getaddress2, getpin, getmobile, getfax, Integer.toString(userModel.getUserID()));
        call.enqueue(new Callback<CustomerResponse>() {
            @Override
            public void onResponse(Call<CustomerResponse> call, Response<CustomerResponse> response) {
                if (response.errorBody() == null) {
                    lblmsg.setText("Address Added.");
                } else {
                    lblmsg.setText("Invalid details.");
                }
            }

            @Override
            public void onFailure(Call<CustomerResponse> call, Throwable throwable) {
                Log.e("eApp", throwable.toString());
            }
        });
    }
}
