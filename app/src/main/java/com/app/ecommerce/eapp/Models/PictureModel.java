package com.app.ecommerce.eapp.Models;

/**
 * Created by RFI on 11-01-2018.
 */

import com.google.gson.annotations.SerializedName;

public class PictureModel
{
    @SerializedName("ImageUrl")
    public String ImageUrl;
    public String ImageUrl() { return ImageUrl; }

    @SerializedName("ThumbImageUrl")
    public String ThumbImageUrl;
    public String ThumbImageUrl() { return ThumbImageUrl; }

    @SerializedName("FullSizeImageUrl")
    public String FullSizeImageUrl;
    public String FullSizeImageUrl() { return FullSizeImageUrl; }

    @SerializedName("Title")
    public String Title;
    public String Title() { return Title; }

    @SerializedName("AlternateText")
    public String AlternateText;
    public String AlternateText() { return AlternateText; }
}