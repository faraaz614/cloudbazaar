package com.app.ecommerce.eapp.Helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.app.ecommerce.eapp.Models.CategoriesProductsViewModel;
import com.app.ecommerce.eapp.Models.ShoppingCartItemViewModel;
import com.app.ecommerce.eapp.Models.UserModel;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by RFI on 22-10-2017.
 */

public class LocalService {
    private Context _context;

    public LocalService(Context context) {
        _context = context;
    }

    public boolean InsertUpdateUser(UserModel userModel) {
        String userid = "";
        SQLiteDatabase myDB = _context.openOrCreateDatabase("eapp.db", MODE_PRIVATE, null);
        Cursor myCursor = myDB.rawQuery("select * from user limit 1", null);

        while (myCursor.moveToNext()) {
            int id = myCursor.getColumnIndex("id");
            userid = myCursor.getString(id);
        }
        myCursor.close();

        if (userid.equals("")) {
            ContentValues row1 = new ContentValues();
            row1.put("name", userModel.getName());
            row1.put("userid", userModel.getUserID());
            myDB.insert("user", null, row1);
            myDB.close();
        } else {
            ContentValues row1 = new ContentValues();
            row1.put("name", userModel.getName());
            row1.put("userid", userModel.getUserID());
            myDB.update("user", row1, "id=" + userid, null);
            myDB.close();
        }
        return true;
    }

    public void SignOut() {
        SQLiteDatabase myDB = _context.openOrCreateDatabase("eapp.db", MODE_PRIVATE, null);
        myDB.execSQL("delete from user");
        Cursor myCursor = myDB.rawQuery("select * from user", null);

        while (myCursor.moveToNext()) {
            int id = myCursor.getColumnIndex("id");
        }
        myCursor.close();
    }

    public UserModel GetUser() {
        UserModel userModel = null;
        SQLiteDatabase myDB = _context.openOrCreateDatabase("eapp.db", MODE_PRIVATE, null);
        Cursor myCursor = myDB.rawQuery("select * from user limit 1", null);

        while (myCursor.moveToNext()) {
            userModel = new UserModel();
            int nameindex = myCursor.getColumnIndex("name");
            int useridindex = myCursor.getColumnIndex("userid");
            userModel.name = myCursor.getString(nameindex);
            userModel.userid = myCursor.getInt(useridindex);
        }
        myCursor.close();
        return userModel;
    }
/*
    public List<CategoriesProductsViewModel> GetUserCart() {
        List<CategoriesProductsViewModel> list = new ArrayList<>();
        SQLiteDatabase myDB = _context.openOrCreateDatabase("eapp.db", MODE_PRIVATE, null);
        Cursor myCursor = myDB.rawQuery("select * from cart", null);

        while (myCursor.moveToNext()) {
            CategoriesProductsViewModel model = new CategoriesProductsViewModel();
            model.SetProductName(myCursor.getString(myCursor.getColumnIndex("productname")));
            model.SetPrice(myCursor.getString(myCursor.getColumnIndex("productprice")));
            model.SetShortDescription(myCursor.getString(myCursor.getColumnIndex("productdesc")));
            model.SetQuantity(myCursor.getString(myCursor.getColumnIndex("quantity")));
            model.SetProductId(myCursor.getInt(myCursor.getColumnIndex("productid")));
            list.add(model);
        }
        myCursor.close();
        return list;
    }

    public boolean InsertUpdateCart(CategoriesProductsViewModel model) {
        int id = 0;
        SQLiteDatabase myDB = _context.openOrCreateDatabase("eapp.db", MODE_PRIVATE, null);
        Cursor myCursor = myDB.rawQuery("select * from cart where productid = " + model.ProductId() + " limit 1", null);

        while (myCursor.moveToNext()) {
            id = myCursor.getInt(myCursor.getColumnIndex("id"));
        }
        myCursor.close();

        if (id == 0) {
            ContentValues row1 = new ContentValues();
            row1.put("productname", model.ProductName());
            row1.put("productprice", model.Price());
            row1.put("productdesc", model.ShortDescription());
            row1.put("quantity", model.Quantity());
            row1.put("productid", model.ProductId());
            myDB.insert("cart", null, row1);
            myDB.close();
        } else {
            ContentValues row1 = new ContentValues();
            row1.put("quantity", model.Quantity());
            myDB.update("cart", row1, "id=" + id, null);
            myDB.close();
        }
        return true;
    }

    public double DeleteFromCart(int productid) {
        int id = 0;
        SQLiteDatabase myDB = _context.openOrCreateDatabase("eapp.db", MODE_PRIVATE, null);
        Cursor myCursor = myDB.rawQuery("select * from cart where productid = " + productid + " limit 1", null);

        while (myCursor.moveToNext()) {
            id = myCursor.getInt(myCursor.getColumnIndex("id"));
        }

        if (id > 0) {
            myDB.delete("cart", "id=" + id, null);
        }
        myDB.close();
        return CartTotal();
    }

    public double CartTotal() {
        double total = 0;
        SQLiteDatabase myDB = _context.openOrCreateDatabase("eapp.db", MODE_PRIVATE, null);
        Cursor myCursor = myDB.rawQuery("select * from cart", null);
        while (myCursor.moveToNext()) {
            total += Double.parseDouble(myCursor.getString(myCursor.getColumnIndex("productprice"))) *
                    Double.parseDouble(myCursor.getString(myCursor.getColumnIndex("quantity")));
        }
        myDB.close();
        return total;
    }

    public int CartCount() {
        int total = 0;
        SQLiteDatabase myDB = _context.openOrCreateDatabase("eapp.db", MODE_PRIVATE, null);
        Cursor myCursor = myDB.rawQuery("select sum(quantity) as totalcount from cart", null);
        while (myCursor.moveToNext()) {
            total = myCursor.getInt(myCursor.getColumnIndex("totalcount"));
        }
        myDB.close();
        return total;
    }*/
}
