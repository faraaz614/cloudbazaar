package com.app.ecommerce.eapp;

/**
 * Created by RFI on 07-11-2017.
 */

public interface BackButtonSupportFragment {
    // return true if your fragment has consumed the back press event, false if you don't care about it
    boolean onBackPressed();
}
