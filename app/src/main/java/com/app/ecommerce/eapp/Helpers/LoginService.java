package com.app.ecommerce.eapp.Helpers;

import com.app.ecommerce.eapp.Models.AddressResponse;
import com.app.ecommerce.eapp.Models.CustomerResponse;
import com.app.ecommerce.eapp.Models.CustomerUpdateResponse;
import com.app.ecommerce.eapp.Models.CustomerViewModel;
import com.app.ecommerce.eapp.Models.SingleAddressResponse;
import com.app.ecommerce.eapp.Models.TEBApiResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by RFI on 24-10-2017.
 */

public interface LoginService {
    @GET("api/User/GetUserDetails")
    Call<CustomerResponse> GetUserDetails(@Query("Email") String Email, @Query("Password") String Password);

    @FormUrlEncoded
    @POST("api/User/PostCustomer")
    Call<CustomerResponse> PostCustomer(@Field("Username") String username,
                                        @Field("Password") String password,
                                        @Field("Email") String email);

    @GET("api/User/GetCustomerAddress")
    Call<AddressResponse> GetCustomerAddress(@Query("CustomerID") int CustomerID);

    @GET("api/User/GetCustomerAddressByID")
    Call<SingleAddressResponse> GetCustomerAddressByID(@Query("AddressID") int AddressID);

    @DELETE("api/User/DeleteCustomerAddress")
    Call<CustomerUpdateResponse> DeleteCustomerAddress(@Query("addressId") int addressId);

    @FormUrlEncoded
    @POST("api/User/AddCustomerAddress")
    Call<CustomerResponse> AddCustomerAddress(@Field("FirstName") String FirstName,
                                              @Field("LastName") String LastName,
                                              @Field("Email") String Email,
                                              @Field("Company") String Company,
                                              @Field("CountryId") String CountryId,
                                              @Field("StateprovinceId") String StateprovinceId,
                                              @Field("City") String City,
                                              @Field("Address1") String Address1,
                                              @Field("Address2") String Address2,
                                              @Field("ZipPostalCode") String ZipPostalCode,
                                              @Field("PhoneNumber") String PhoneNumber,
                                              @Field("FaxNumber") String FaxNumber,
                                              @Field("CustomerID") String CustomerID);

    @FormUrlEncoded
    @PUT("api/User/UpdateCustomerAddress")
    Call<CustomerUpdateResponse> UpdateCustomerAddress(@Field("FirstName") String FirstName,
                                                       @Field("LastName") String LastName,
                                                       @Field("Email") String Email,
                                                       @Field("Company") String Company,
                                                       @Field("CountryId") String CountryId,
                                                       @Field("StateprovinceId") String StateprovinceId,
                                                       @Field("City") String City,
                                                       @Field("Address1") String Address1,
                                                       @Field("Address2") String Address2,
                                                       @Field("ZipPostalCode") String ZipPostalCode,
                                                       @Field("PhoneNumber") String PhoneNumber,
                                                       @Field("FaxNumber") String FaxNumber,
                                                       @Field("Id") int Id);
}
