package com.app.ecommerce.eapp.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RFI on 09-11-2017.
 */

public class SingleAddressResponse {
    @SerializedName("IsSuccess")
    private boolean IsSuccess;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private AddressViewModel Data;

    public boolean IsSuccess() {
        return IsSuccess;
    }

    public String Message() {
        return Message;
    }

    public AddressViewModel Data() {
        return Data;
    }
}
