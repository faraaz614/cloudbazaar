package com.app.ecommerce.eapp.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RFI on 03-12-2017.
 */

public class OrdersResponse {
    @SerializedName("IsSuccess")
    private boolean IsSuccess;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<OrderViewModel> Data;

    public boolean IsSuccess() {
        return IsSuccess;
    }

    public String Message() {
        return Message;
    }

    public List<OrderViewModel> Data() {
        return Data;
    }
}
